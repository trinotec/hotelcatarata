﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelCatarataCore.Web.Helpers;
using HotelCatarataCore.Web.Models;
using HotelCatarataCore.Web.Services;
using Microsoft.AspNetCore.Mvc;

namespace HotelCatarataCore.Web.Controllers
{
    public class HabitacionesController : Controller
    {
        private readonly IApiService _apiService;
        public HabitacionesController(IApiService apiService)
        {
            _apiService = apiService;
        }

        public async Task<IActionResult> Index(string FechaIniCheck = null, string fechaFinCheck = null, int huespedes = 1, int children = 0)
        {
            try
            {

                //informacion de Hotel  ------------------------------
                var url = Constants.UrlAPI;
                var idCodeHotel = Constants.IdCodeHotel;
                var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);

                if (responseConexionHotel.Result != null)
                {
                    if (responseConexionHotel.Result.Count > 0)
                    {
                        ViewBag.DataHotel = responseConexionHotel.Result[0];
                        ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                        ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                    }
                }


                //informacion de acceso ------------------------------     
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;


                //informacion de precios---------------------------
                var responsePrecios= await _apiService.GetRoomPrice(url, Constants.UrlAPP, "/api/HabitacionesTipos/GetPrices", _dataAccess?.token_type, _dataAccess?.access_token);                
                ViewBag.PreciosHabitaciones = responsePrecios.Result;

                //Footer = 
                var responseGeneral = await _apiService.GetDatosGeneral(url, Constants.UrlAPP, "/api/General/GetFullInfo", _dataAccess?.token_type, _dataAccess?.access_token);

                if (responseGeneral.IsSuccess)
                {
                    if (responseGeneral.Result.General.Count > 0)
                    {
                        ViewBag.Footer = responseGeneral.Result.General[0];
                        ViewBag.FooterContacto = responseGeneral.Result.Contacto[0];
                    }
                }

            }
            catch
            {

            }            

            //return View();
            return await Task.Run(() => View());
        }

        [HttpGet]
        public async Task<IActionResult> BusquedaHabitaciones(int NumPagina, string PrecioFiltro, int NumHabPag)
        {
            Response<HabitacionesResponse> ConsultaHabitaciones = new Response<HabitacionesResponse>();             
            try
            {
                //informacion de Hotel  ------------------------------
                var url = Constants.UrlAPI;
                var idCodeHotel = Constants.IdCodeHotel;
                var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);

                if (responseConexionHotel.Result != null)
                {
                    if (responseConexionHotel.Result.Count > 0)
                    {
                        ViewBag.DataHotel = responseConexionHotel.Result[0];
                        ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                        ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                    }
                }


                //informacion de acceso ------------------------------     
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;

                //informacion de habitaciones---------------------------

                PrecioFiltro = PrecioFiltro.Substring(1, (PrecioFiltro.Length - 1));
                double precionEnviar = 0;
                double PrecioHabitacionFiltro;
                if (double.TryParse(PrecioFiltro, out PrecioHabitacionFiltro))
                {
                    precionEnviar = PrecioHabitacionFiltro;
                }


                var requestHabitaciones = new HomeBannersRequest()
                {
                    Take = NumHabPag,
                    Skip = (NumPagina * NumHabPag - NumHabPag),                    
                    SearchText = "",
                    Activo = true,
                    RoomPrice = precionEnviar
                };

                var responseHabitaciones = await _apiService.GetHabitaciones(url, Constants.UrlAPP, "/api/HabitacionesTipos", _dataAccess?.token_type, _dataAccess?.access_token, requestHabitaciones);
                if (responseHabitaciones.IsSuccess)
                {
                    ConsultaHabitaciones = responseHabitaciones;
                }
                

            }
            catch
            {

            }

            return Json(ConsultaHabitaciones);
        }
    }
}