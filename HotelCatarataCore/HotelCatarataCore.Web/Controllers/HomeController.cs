﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HotelCatarataCore.Web.Models;
using HotelCatarataCore.Web.Services;
using HotelCatarataCore.Web.Helpers;
using System.IO;
using System.Web;
using System.Net;
using Newtonsoft.Json;

namespace HotelCatarataCore.Web.Controllers
{
    

    public class HomeController : Controller
    {
        private readonly IApiService _apiService;
        
        private readonly ILogger<HomeController> _logger;

        

        public HomeController(ILogger<HomeController> logger, IApiService apiService)
        {
            _logger = logger;
            _apiService = apiService;
        }

        public async Task<IActionResult> Index()
        {
            
            List<HomeBanner> homeBanners = new List<HomeBanner>();
            List<HomeBannersSecundario> homeBannerSecundario = new List<HomeBannersSecundario>() ;
            ViewBag.MesActFormat = HelpMonth.ObtenerMes(DateTime.Now.Month);
            ViewBag.MesActFormatSalida = HelpMonth.ObtenerMes(DateTime.Now.AddDays(1).Month);
            try
            {

                //informacion de Hotel  ------------------------------
                var url = Constants.UrlAPI;
                var idCodeHotel = Constants.IdCodeHotel;
                var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);

                if (responseConexionHotel.Result != null)
                {
                    if (responseConexionHotel.Result.Count > 0)
                    {
                        ViewBag.DataHotel = responseConexionHotel.Result[0];
                        ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                        ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                    }
                }


                //informacion de acceso ------------------------------     
                var acceso = GetAccess.Instance(_apiService).GetToken(); 
                var _dataAccess = acceso.Result;                

                //informacion del home banner -------------------------
                var requestHomeBanner = new HomeBannersRequest()
                {
                    Take = 1,
                    Skip = 0,
                    SearchText = "",
                    Activo = true
                };

                var responseHomeBanner = await _apiService.GetHomeBanners(url, Constants.UrlAPP, "/api/HomeBanners", _dataAccess?.token_type, _dataAccess?.access_token, requestHomeBanner);                

                if (responseHomeBanner.IsSuccess)
                {
                    List<HomeBanner> HomeBanner = new List<HomeBanner>();
                    int contId = 0;
                    if(responseHomeBanner.Result.Count > 0)
                    {
                        if (responseHomeBanner.Result[0].ImageFullPath != null && ImageExist(responseHomeBanner.Result[0].ImageFullPath))
                        {
                            HomeBanner.Add(new HomeBanner
                            {
                                Id = contId,
                                ImageFullPath = responseHomeBanner.Result[0].ImageFullPath,
                                Texto1 = responseHomeBanner.Result[0].Texto1,
                                Texto2 = responseHomeBanner.Result[0].Texto2
                            });
                            contId++;
                        }
                        if (responseHomeBanner.Result[0].Image1FullPath != null && ImageExist(responseHomeBanner.Result[0].Image1FullPath))
                        {
                            HomeBanner.Add(new HomeBanner
                            {
                                Id = contId,
                                ImageFullPath = responseHomeBanner.Result[0].Image1FullPath,
                                Texto1 = responseHomeBanner.Result[0].Texto1,
                                Texto2 = responseHomeBanner.Result[0].Texto2
                            });
                            contId++;
                        }
                        if (responseHomeBanner.Result[0].Image2FullPath != null && ImageExist(responseHomeBanner.Result[0].Image2FullPath))
                        {
                            HomeBanner.Add(new HomeBanner
                            {
                                Id = contId,
                                ImageFullPath = responseHomeBanner.Result[0].Image2FullPath,
                                Texto1 = responseHomeBanner.Result[0].Texto1,
                                Texto2 = responseHomeBanner.Result[0].Texto2
                            });
                        }
                        ViewData["HomeBanner"] = HomeBanner;
                    }

                }


                //informacion home banner secundario --------------------                
                var requestBannerSecundario = new HomeBannersRequest()
                {
                    Take = 1,
                    Skip = 0,
                    SearchText = "",
                    Activo = true
                };

                var responseBannerSecundairo = await _apiService.GetHomeBannersSecundario(url, Constants.UrlAPP, "/api/HomeBannersSecundario", _dataAccess?.token_type, _dataAccess?.access_token, requestBannerSecundario);
                if(responseBannerSecundairo.Result != null)
                {
                    ViewBag.HommebannerSec = (responseBannerSecundairo.Result.Count > 0) ? responseBannerSecundairo.Result[0] : null;
                }
                
                //informacion home servicios -----------------------------
                var requestBannerServicioss = new HomeBannersRequest()
                {
                    Take = 4,
                    Skip = 0,
                    SearchText = "",
                    Activo = true
                };
                var responseHomeServices= await _apiService.GetHomeServices(url, Constants.UrlAPP, "/api/HomeServicios", _dataAccess?.token_type, _dataAccess?.access_token, requestBannerServicioss);
                if (responseHomeServices.IsSuccess)
                {
                    int count = 0;
                    foreach (var serv in responseHomeServices.Result)
                    {
                        serv.NumClass = count % 2;
                        count++;
                    }

                    ViewBag.HomeServices = responseHomeServices.Result;
                    //return;
                }
                

                //informacion de habitaciones---------------------------
                var requestHabitaciones = new HomeBannersRequest()
                {
                    Take = 5,
                    Skip = 0,
                    SearchText = "",
                    Activo = true
                };

                var responseHabitaciones = await _apiService.GetHabitaciones(url, Constants.UrlAPP, "/api/HabitacionesTipos", _dataAccess?.token_type, _dataAccess?.access_token, requestHabitaciones);
                if (responseHabitaciones.Result != null)
                {
                    ViewBag.Habitaciones = responseHabitaciones.Result.listaHabitaciones;


                    ViewBag.TotalHabitacionesHome = ViewBag.Habitaciones.Count;

                    if (ViewBag.Habitaciones.Count >= 3)
                    {
                        ViewBag.ClassSingleroom = "col-lg-4 col-md-6";
                    }
                    else if (ViewBag.Habitaciones.Count == 2)
                    {   
                        ViewBag.ClassSingleroom = "col-lg-4 col-md-6 col2Habitaciones";
                    }
                    else if(ViewBag.Habitaciones.Count == 1)
                    {
                        ViewBag.ClassSingleroom = "col1Habitaciones";
                    }
                    
                }
                else
                {

                }


                //informacion del home video -------------------------
                var requestHomeVideo= new HomeBannersRequest()
                {
                    Take = 1,
                    Skip = 0,
                    SearchText = "",
                    Activo = true
                };

                var responseHomeVideo = await _apiService.GetHomeVideo(url, Constants.UrlAPP, "/api/HomeVideos", _dataAccess?.token_type, _dataAccess?.access_token, requestHomeBanner);

                if (responseHomeVideo.Result != null)
                {
                    if(!string.IsNullOrEmpty(responseHomeVideo.Result[0].Url))
                    {                        
                        string WatchVideo = responseHomeVideo.Result[0].Url.Substring(responseHomeVideo.Result[0].Url.IndexOf("?v=")+3);
                        responseHomeVideo.Result[0].Url = (WatchVideo.IndexOf("&") != -1) ? WatchVideo.Substring(0, WatchVideo.IndexOf("&")): WatchVideo ;                        
                    }                    
                    ViewBag.HomeVideo = responseHomeVideo?.Result[0];
                }
 
                //Footer = 
                var responseGeneral = await _apiService.GetDatosGeneral(url, Constants.UrlAPP, "/api/General/GetFullInfo", _dataAccess?.token_type, _dataAccess?.access_token);
                if (responseGeneral.IsSuccess)
                {
                    ViewBag.Footer = responseGeneral.Result.General[0];
                    ViewBag.FooterContacto = responseGeneral.Result.Contacto[0];
                }

            }
            catch (Exception ex)
            { 
 
            }
            return await Task.Run(() => View());

        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        public bool ImageExist(string url)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "HEAD";
            try
            {
                request.GetResponse();
                return  true;
            }
            catch
            {
                return false;
            }
        }

    }
}
;