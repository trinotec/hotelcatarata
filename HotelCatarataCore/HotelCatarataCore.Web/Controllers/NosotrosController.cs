﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelCatarataCore.Web.Models;
using Microsoft.AspNetCore.Mvc;
using HotelCatarataCore.Web.Services;
using HotelCatarataCore.Web.Helpers;

namespace HotelCatarataCore.Web.Controllers
{
    public class NosotrosController : Controller
    {

        public NosotrosController(IApiService apiService)
        {
            _apiService = apiService;
        }

        private readonly IApiService _apiService;
        public async Task<IActionResult> Index()
        {
            
            try
            {
                //informacion de Hotel  ------------------------------
                var url = Constants.UrlAPI;
                var idCodeHotel = Constants.IdCodeHotel;
                var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);

                if (responseConexionHotel.Result != null)
                {
                    if (responseConexionHotel.Result.Count > 0)
                    {
                        ViewBag.DataHotel = responseConexionHotel.Result[0];
                        ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                        ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                    }
                }


                //informacion de acceso ------------------------------     
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;

                //informacion de Nosotros ---------------------------
                var requestNosotros = new HomeBannersRequest()
                {
                    Take = 1,
                    Skip = 0,
                    SearchText = "",
                    Activo = true
                };

                var responseNosotros = await _apiService.GetNosotros(url, Constants.UrlAPP, "/api/Nosotros", _dataAccess?.token_type, _dataAccess?.access_token, requestNosotros);
                if (responseNosotros.Result != null)
                {
                    if (!string.IsNullOrEmpty(responseNosotros.Result[0].UrlVideo))
                    {
                        string WatchVideo = responseNosotros.Result[0].UrlVideo.Substring(responseNosotros.Result[0].UrlVideo.IndexOf("?v=") + 3);
                        responseNosotros.Result[0].UrlVideo = (WatchVideo.IndexOf("&") != -1) ? WatchVideo.Substring(0, WatchVideo.IndexOf("&")) : WatchVideo;
                    }
                    ViewBag.Nosotros = responseNosotros.Result[0];
                    ViewBag.TituloPagina = responseNosotros.Result[0].TituloPagina;
                    ViewBag.Metadata = responseNosotros.Result[0].MetaPagina;

                }


                //informacion de Nosotros Comentarios ---------------------------
                var requestNosotrosComentarios = new HomeBannersRequest()
                {
                    Take = 10,
                    Skip = 0,
                    SearchText = "",
                    Activo = true
                };

                var responseNosotrosComentarios = await _apiService.GetNosotrosComentarios(url, Constants.UrlAPP, "/api/NosotrosComentarios", _dataAccess?.token_type, _dataAccess?.access_token, requestNosotrosComentarios);
                if (responseNosotrosComentarios.IsSuccess)
                {
                    if (responseNosotrosComentarios.Result.Count>0)
                    {
                        foreach(var comentario in responseNosotrosComentarios.Result)
                        {
                            if(string.IsNullOrEmpty(comentario.FotoFullPath))
                            {
                                comentario.FotoFullPath = "/images/Nosotros/userDefault.png";
                            }
                        }
                    }

                    ViewBag.NosotrosComentarios = responseNosotrosComentarios.Result;
                }
                ViewBag.NosotrosComentarios = responseNosotrosComentarios.Result;


                //Footer = 
                var responseGeneral = await _apiService.GetDatosGeneral(url, Constants.UrlAPP, "/api/General/GetFullInfo", _dataAccess?.token_type, _dataAccess?.access_token);

                if (responseGeneral.IsSuccess)
                {
                    if (responseGeneral.Result.General.Count > 0)
                    {
                        ViewBag.Footer = responseGeneral.Result.General[0];
                        ViewBag.FooterContacto = responseGeneral.Result.Contacto[0];
                    }
                }

            }
            catch
            {

            }
            
            return await Task.Run(() => View());
        }


        public async Task<IActionResult> Terminos()
        {
            //informacion de Hotel  ------------------------------
            var url = Constants.UrlAPI;
            var idCodeHotel = Constants.IdCodeHotel;
            var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);

            if (responseConexionHotel.Result != null)
            {
                if (responseConexionHotel.Result.Count > 0)
                {
                    ViewBag.DataHotel = responseConexionHotel.Result[0];
                    ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                    ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                }
            }


            //informacion de acceso ------------------------------     
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;



            //-----------------------
            //banner home para header
            var requestHomeBanner = new HomeBannersRequest()
            {
                Take = 1,
                Skip = 0,
                SearchText = "",
                Activo = true
            };
            var responseHomeBanner = await _apiService.GetHomeBanners(url, Constants.UrlAPP, "/api/HomeBanners", _dataAccess?.token_type, _dataAccess?.access_token, requestHomeBanner);

            if (responseHomeBanner.Result != null)
            {
                ViewBag.ImageHeader = responseHomeBanner.Result[0].ImageFullPath;
            }
            else
            {
                ViewBag.ImageHeader = "";
            }



            //Footer = 
            var responseGeneral = await _apiService.GetDatosGeneral(url, Constants.UrlAPP, "/api/General/GetFullInfo", _dataAccess?.token_type, _dataAccess?.access_token);

            if (responseGeneral.Result != null)
            {
                if (responseGeneral.Result.General.Count > 0)
                {
                    ViewBag.Footer = responseGeneral.Result.General[0];
                    ViewBag.FooterContacto = responseGeneral.Result.Contacto[0];
                }
            }

            return await Task.Run(() => View());
        }

        public async Task<IActionResult> PoliticasPrivacidad()
        {
            //informacion de Hotel  ------------------------------
            var url = Constants.UrlAPI;
            var idCodeHotel = Constants.IdCodeHotel;
            var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);

            if (responseConexionHotel.Result != null)
            {
                if (responseConexionHotel.Result.Count > 0)
                {
                    ViewBag.DataHotel = responseConexionHotel.Result[0];
                    ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                    ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                }
            }


            //informacion de acceso ------------------------------     
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;


            //-----------------------
            //banner home para header
            var requestHomeBanner = new HomeBannersRequest()
            {
                Take = 1,
                Skip = 0,
                SearchText = "",
                Activo = true
            };
            var responseHomeBanner = await _apiService.GetHomeBanners(url, Constants.UrlAPP, "/api/HomeBanners", _dataAccess?.token_type, _dataAccess?.access_token, requestHomeBanner);

            if (responseHomeBanner.Result != null)
            {
                ViewBag.ImageHeader = responseHomeBanner.Result[0].ImageFullPath;
            }
            else
            {
                ViewBag.ImageHeader = "";
            }

            //-----------------------
            //Footer = 
            var responseGeneral = await _apiService.GetDatosGeneral(url, Constants.UrlAPP, "/api/General/GetFullInfo", _dataAccess?.token_type, _dataAccess?.access_token);

            if (responseGeneral.Result != null)
            {
                if (responseGeneral.Result.General.Count > 0)
                {
                    ViewBag.Footer = responseGeneral.Result.General[0];
                    ViewBag.FooterContacto = responseGeneral.Result.Contacto[0];
                }
            }

            return await Task.Run(() => View());
        }

        public async Task<IActionResult> PoliticasCancelacion()
        {
            //informacion de Hotel  ------------------------------
            var url = Constants.UrlAPI;
            var idCodeHotel = Constants.IdCodeHotel;
            var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);

            if (responseConexionHotel.Result != null)
            {
                if (responseConexionHotel.Result.Count > 0)
                {
                    ViewBag.DataHotel = responseConexionHotel.Result[0];
                    ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                    ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                }
            }


            //informacion de acceso ------------------------------     
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;

            //-----------------------
            //banner home para header
            var requestHomeBanner = new HomeBannersRequest()
            {
                Take = 1,
                Skip = 0,
                SearchText = "",
                Activo = true
            };
            var responseHomeBanner = await _apiService.GetHomeBanners(url, Constants.UrlAPP, "/api/HomeBanners", _dataAccess?.token_type, _dataAccess?.access_token, requestHomeBanner);

            if (responseHomeBanner.Result != null)
            {
                ViewBag.ImageHeader = responseHomeBanner.Result[0].ImageFullPath;
            }
            else
            {
                ViewBag.ImageHeader = "";
            }

            //-----------------------
            //Footer = 
            var responseGeneral = await _apiService.GetDatosGeneral(url, Constants.UrlAPP, "/api/General/GetFullInfo", _dataAccess?.token_type, _dataAccess?.access_token);

            if (responseGeneral.Result != null)
            {
                if (responseGeneral.Result.General.Count > 0)
                {
                    ViewBag.Footer = responseGeneral.Result.General[0];
                    ViewBag.FooterContacto = responseGeneral.Result.Contacto[0];
                }
            }

            return await Task.Run(() => View());
        }
    }
}