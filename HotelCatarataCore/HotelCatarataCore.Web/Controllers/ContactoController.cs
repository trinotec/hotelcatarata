﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using HotelCatarataCore.Web.Helpers;
using HotelCatarataCore.Web.Models;
using HotelCatarataCore.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HotelCatarataCore.Web.Controllers
{
    public class ContactoController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IApiService _apiService;
        private readonly IEmailSender _emailSender;

        public ContactoController(ILogger<HomeController> logger, IApiService apiService, IEmailSender emailSender)
        {
            _logger = logger;
            _apiService = apiService;
            _emailSender = emailSender;
        }

        public async Task<IActionResult> Index()
        {            
            try
            {
                //informacion de Hotel  ------------------------------
                var url = Constants.UrlAPI;
                var idCodeHotel = Constants.IdCodeHotel;
                var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);

                if (responseConexionHotel.Result != null)
                {
                    if(responseConexionHotel.Result.Count > 0)
                    {
                        ViewBag.DataHotel = responseConexionHotel.Result[0];
                        ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                        ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                    }
                }


                //informacion de acceso ------------------------------     
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;



                //informacion de contacto--------------------------
                var responseContacto = await _apiService.GetContact(url, Constants.UrlAPP, "/api/Contacto", _dataAccess?.token_type, _dataAccess?.access_token);    
                
                if(responseContacto.Result != null)
                {                    
                    if(responseContacto.Result.Count > 0)
                    {
                        ViewBag.Contacto = responseContacto.Result[0];
                        ViewBag.TituloPagina = responseContacto.Result[0].TituloPagina;
                        ViewBag.Metadata = responseContacto.Result[0].MetaPagina;
                    }
                }
                

                //Footer = 
                var responseGeneral = await _apiService.GetDatosGeneral(url, Constants.UrlAPP, "/api/General/GetFullInfo", _dataAccess?.token_type, _dataAccess?.access_token);

                if (responseGeneral.IsSuccess)
                {
                    if(responseGeneral.Result.General.Count> 0)
                    {
                        ViewBag.Footer = responseGeneral.Result.General[0];
                        ViewBag.FooterContacto = responseGeneral.Result.Contacto[0];
                    }                     
                }


            }
            catch
            {

            }


            return await Task.Run(() => View());
        }


        [HttpPost]
        public async Task<IActionResult> EnviarCorreo(MensajeContacto data)        
        {
            
            string Mensaje = "<b>Asunto: </b> " + data.Asunto + "<br>";
            Mensaje += "<b>Nombre: </b>" + data.NomCliente + "<br>";
            Mensaje += "<b>Correo Electronico: </b>" + data.Email + "<br>";
            Mensaje += "<b>Mensaje: </b>" + data.Mensaje + "<br>";

            try
            {
                await _emailSender.SendEmailAsync(data.EmailHotel, data.Asunto, Mensaje).ConfigureAwait(false);
                return Json(new { IsSuccess = true });
            }
            catch(Exception ex)
            {
                var msg = ex.Message;
                return Json(new { IsSuccess = false });
            }

            return Json(new{ IsSuccess  =true});
        }


   
    }
}