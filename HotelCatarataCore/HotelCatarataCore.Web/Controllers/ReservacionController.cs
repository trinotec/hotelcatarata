﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelCatarataCore.Web.Helpers;
using HotelCatarataCore.Web.Models;
using HotelCatarataCore.Web.Services;
using Microsoft.AspNetCore.Mvc;

namespace HotelCatarataCore.Web.Controllers
{
    public class ReservacionController : Controller
    {
        private readonly IApiService _apiService;
        public ReservacionController(IApiService apiService)
        {
            _apiService = apiService;
        }

        public async Task<IActionResult> Index(string FechaIniCheck = null, string fechaFinCheck = null, int huespedes = 1, int children = 0)
        {
            try
            {

                //informacion de Hotel  ------------------------------
                var url = Constants.UrlAPI;
                var idCodeHotel = Constants.IdCodeHotel;
                var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);

                if (responseConexionHotel.Result != null)
                {
                    if (responseConexionHotel.Result.Count > 0)
                    {
                        ViewBag.DataHotel = responseConexionHotel.Result[0];
                        ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                        ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                    }
                }


                //informacion de acceso ------------------------------     
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;

                //informacion de precios---------------------------
                var responsePrecios = await _apiService.GetRoomPrice(url, Constants.UrlAPP, "/api/HabitacionesTipos/GetPrices", _dataAccess?.token_type, _dataAccess?.access_token);
                ViewBag.PreciosHabitaciones = responsePrecios.Result;

                //Footer = 
                var responseGeneral = await _apiService.GetDatosGeneral(url, Constants.UrlAPP, "/api/General/GetFullInfo", _dataAccess?.token_type, _dataAccess?.access_token);

                if (responseGeneral.IsSuccess)
                {
                    if (responseGeneral.Result.General.Count > 0)
                    {
                        ViewBag.Footer = responseGeneral.Result.General[0];
                        ViewBag.FooterContacto = responseGeneral.Result.Contacto[0];
                    }
                }

            }
            catch
            {

            }

            if (FechaIniCheck == null || fechaFinCheck == null)
            {
                ViewBag.FechaIniCheck = DateTime.Now.ToString("yyyy-MM-dd");
                ViewBag.fechaFinCheck = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                ViewBag.huespedes = 1;
                ViewBag.ninos = 0;
            }
            else
            {
                ViewBag.FechaIniCheck = FechaIniCheck;
                ViewBag.fechaFinCheck = fechaFinCheck;
                ViewBag.huespedes = huespedes;
                ViewBag.ninos = children;
            }


            //return View();
            return await Task.Run(() => View());
        }

        [HttpGet]
        public async Task<IActionResult> BusquedaHabitaciones(string FechaInicio, string FechaFinal, int TotalHuespedes, int children)
        {

            try
            {
                //informacion de Hotel  ------------------------------
                var url = Constants.UrlAPI;
                var idCodeHotel = Constants.IdCodeHotel;
                var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);


                //informacion de acceso ------------------------------                
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;

                //informacion de habitaciones--------------------------- 

                var requestHabitaciones = new BannersRequest()
                {
                    Take = 100,
                    Skip = 0, // (NumPagina * 3 - 3),
                    FechaInicio = FechaInicio,
                    FechaFinal = FechaFinal,
                    CantAdultos = TotalHuespedes,
                    CantNinos = children
                };

                var responseHabitaciones = await _apiService.GetRoomsavailable(url, Constants.UrlAPP, "/api/HabitacionesTipos/GetAvailability", _dataAccess?.token_type, _dataAccess?.access_token, requestHabitaciones);
                
                if (responseHabitaciones.IsSuccess)
                {

                    List<double> ListaPrecio = new List<double>();
                    List<Servicio> ServiciosBusqueda = new List<Servicio>();
                    foreach (var habitacion in responseHabitaciones.Result.listaHabitaciones)
                    {
                        ServiciosBusqueda = habitacion.Servicios.FindAll(x => !ServiciosBusqueda.Contains(x)).ToList();

                        if (!ListaPrecio.Contains(habitacion.Precio))
                        {
                            ListaPrecio.Add(habitacion.Precio);
                        }
                    }
                    List<double> ListaPrecioOrdenada = ListaPrecio.OrderBy(x => x).ToList(); ;

                    var enviar = new
                    {
                        IsSuccess = true,
                        Result = responseHabitaciones.Result,
                        ServBusqueda = ServiciosBusqueda,
                        PreciosHabitaciones = ListaPrecioOrdenada
                    };
                    return Json(enviar);
                }
                else
                {
                    return Json(new { IsSuccess = false , Msj = responseHabitaciones.Message});
                }

            }
            catch
            {

            }

            return Json(new { IsSuccess = false });
        }




    }
}