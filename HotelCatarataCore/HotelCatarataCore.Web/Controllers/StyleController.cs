﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace HotelCatarataCore.Web.Controllers
{
    public class StyleController : Controller
    {
        public async Task<IActionResult> Index(string id)
        {
            ViewBag.ColorPrimary = id;
            System.Drawing.Color colorx = System.Drawing.ColorTranslator.FromHtml(id);
           


            return await this.GetPlainCssAsync("Index");
        }
    }
}