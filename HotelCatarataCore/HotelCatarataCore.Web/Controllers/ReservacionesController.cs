﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using HotelCatarataCore.Web.Helpers;
using HotelCatarataCore.Web.Models;
using HotelCatarataCore.Web.Services;
using Microsoft.AspNetCore.Mvc;

namespace HotelCatarataCore.Web.Controllers
{    
    public class ReservacionesController : Controller
    {
        private readonly IApiService _apiService;
        public ReservacionesController(IApiService apiService)
        {
            _apiService = apiService;
        }

       [HttpGet]
        public async Task<IActionResult> Index(string FechaIniCheck = null, string  fechaFinCheck = null, int huespedes = 1)
        {
            DateTime DtFechaini;
            DateTime DtFechaFin;            
            if (DateTime.TryParse(FechaIniCheck, out DtFechaini) && DateTime.TryParse(fechaFinCheck , out DtFechaFin))
            {
                               

                ViewBag.DiaIni = DtFechaini.Day;
                ViewBag.MesActFormat = HelpMonth.ObtenerMes(DtFechaini.Month);
                ViewBag.YearIni = DtFechaini.Year;

                ViewBag.DiaFin = DtFechaFin.Day;
                ViewBag.MesActFormatSalida = HelpMonth.ObtenerMes(DtFechaFin.Month);
                ViewBag.YearFin = DtFechaFin.Year;
                ViewBag.Huespedes = huespedes;
                
            }
            else
            {
                ViewBag.DiaIni = DateTime.Now.Day;
                ViewBag.MesActFormat = HelpMonth.ObtenerMes(DateTime.Now.Month);
                ViewBag.YearIni = DateTime.Now.Year;

                ViewBag.DiaFin = DateTime.Now.AddDays(1).Day;
                ViewBag.MesActFormatSalida = HelpMonth.ObtenerMes(DateTime.Now.AddDays(1).Month);
                ViewBag.YearFin = DateTime.Now.AddDays(1).Year;
                ViewBag.Huespedes = 1;
            }


            try
            {
                //informacion de Hotel  ------------------------------
                var url = Constants.UrlAPI;
                var idCodeHotel = Constants.IdCodeHotel;
                var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);

                if (responseConexionHotel.Result != null)
                {
                    if (responseConexionHotel.Result.Count > 0)
                    {
                        ViewBag.DataHotel = responseConexionHotel.Result[0];
                        ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                        ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                    }
                }


                //informacion de acceso ------------------------------     
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;


                //Footer = 
                var responseGeneral = await _apiService.GetDatosGeneral(url, Constants.UrlAPP, "/api/General/GetFullInfo", _dataAccess.token_type, _dataAccess.access_token);

                if (responseGeneral.IsSuccess)
                {
                    if (responseGeneral.Result.General.Count > 0)
                    {
                        ViewBag.Footer = responseGeneral.Result.General[0];
                        ViewBag.FooterContacto = responseGeneral.Result.Contacto[0];
                    }
                }

                
            }
            catch
            {

            }

            //return View();
            return await Task.Run(() => View());
            
        }


        [HttpGet]
        public async Task<IActionResult> BusquedaHabitaciones(string FechaInicio, string FechaFinal, int TotalHuespedes)
        {
            
            try
            {
                //informacion de acceso ------------------------------
                var url = Constants.UrlAPI;
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;

                //informacion de habitaciones--------------------------- 

                var requestHabitaciones = new BannersRequest()
                {
                    Take = 100,
                    Skip = 0, // (NumPagina * 3 - 3),
                    FechaInicio = FechaInicio,
                    FechaFinal = FechaFinal,
                    CantAdultos = TotalHuespedes,
                    CantNinos = 0                    
                };

                var responseHabitaciones = await _apiService.GetRoomsavailable(url, Constants.UrlAPP, "/api/HabitacionesTipos/GetAvailability", _dataAccess.token_type, _dataAccess.access_token, requestHabitaciones);
                if (responseHabitaciones.IsSuccess)
                {                                 

                    List<double> ListaPrecio = new List<double>();
                    List<Servicio> ServiciosBusqueda = new List<Servicio>();
                    foreach (var habitacion in responseHabitaciones.Result.listaHabitaciones)
                    {
                        ServiciosBusqueda = habitacion.Servicios.FindAll(x => !ServiciosBusqueda.Contains(x)).ToList();

                        if(!ListaPrecio.Contains(habitacion.Precio))
                        {
                            ListaPrecio.Add(habitacion.Precio);
                        }                            
                    }
                    List<double> ListaPrecioOrdenada = ListaPrecio.OrderBy(x => x).ToList(); ;

                    var enviar = new
                    {
                        IsSuccess = true,
                        Result = responseHabitaciones.Result,
                        ServBusqueda = ServiciosBusqueda,
                        PreciosHabitaciones = ListaPrecioOrdenada
                    };
                    return Json(enviar);
                }


            }
            catch
            {

            }

            return Json(new { IsSuccess = false });
        }

        [HttpPost]
        public async Task<IActionResult> RealizarReserva(string CodTipoHab, string FechaLlegada, string FechaSalida, int huespedes, double TotalReservacion)
        {

            string codigoHabitacion = (Int32.Parse(CodTipoHab)).ToString("00");
            DateTime fecha1 = DateTime.Parse(FechaLlegada);
            DateTime fecha2 = DateTime.Parse(FechaSalida);            
            int diasEstancia = (int)((fecha2 - fecha1).TotalDays);
            var precioNoche = TotalReservacion / diasEstancia;

            try
            {
                //informacion de acceso ------------------------------
                var url = Constants.UrlAPI;
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;

                //Realizo reservacion--------------------------- 
                /*
                var requestHabitaciones = new RequestReservacion()
                {
                    CodTipoHab = codigoHabitacion,
                    FechaLlegada = FechaLlegada,
                    FechaSalida = FechaSalida,
                    NumNoches = diasEstancia,
                    PrecioNoche = precioNoche,
                    Nombres = "Yessenia y Carlos",
                    Apellidos = "Rojas Aguirre",
                    Telefonos = "8323-9798",
                    NumIdentificacion = "1-0125-8899",
                    CodPais = "506",
                    CodCiudad = "ALJ",
                    Email = "info@sicsoftsa.com",
                    Empresa = "",
                    Comentarios = "",
                    Adultos = huespedes,
                    Ninos = 0,
                    StatusReserva = "1",
                    MontoTotalReserva = TotalReservacion,
                    MontoPagado = 60.0000,
                    PagoEfectivo = 0.0000,
                    PagoTarjeta = 60.0000,
                    PagoTransferencia = 0.0000,
                    AutorizacionTarjeta = "",
                    ReferenciaTarjeta = null,
                    NumTransferencia = null
                }; 
                var responseReservacion = await _apiService.PostReservacion(url, Constants.UrlAPP, "/api/Reservas", _dataAccess.token_type, _dataAccess.access_token, requestHabitaciones);
                
                if (responseReservacion.IsSuccess)
                {
                    return Json(responseReservacion);
                }*/
            }
            catch
            {

            }
            var Enviar = new
            {
                IsSuccess = "False"
            };
            return Json(Enviar);
 
        }


    }
}