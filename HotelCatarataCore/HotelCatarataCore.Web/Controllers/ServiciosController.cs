﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelCatarataCore.Web.Helpers;
using HotelCatarataCore.Web.Models;
using HotelCatarataCore.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HotelCatarataCore.Web.Controllers
{
    public class ServiciosController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IApiService _apiService;


        public ServiciosController(ILogger<HomeController> logger, IApiService apiService)
        {
            _logger = logger;
            _apiService = apiService;
        }

        public async Task<IActionResult> Index()
        {
            try
            {
                //informacion de Hotel  ------------------------------
                var url = Constants.UrlAPI;
                var idCodeHotel = Constants.IdCodeHotel;
                var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);

                if (responseConexionHotel.Result != null)
                {
                    if (responseConexionHotel.Result.Count > 0)
                    {
                        ViewBag.DataHotel = responseConexionHotel.Result[0];
                        ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                        ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                    }
                }


                //informacion de acceso ------------------------------     
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;


                //informacion de servicio ------------------------------    
                var responseServicio = await _apiService.GetDatosServicios(url, Constants.UrlAPP, "/api/serviciosPage", _dataAccess.token_type, _dataAccess.access_token);

                if(responseServicio.Result != null)
                {
                    ViewBag.Servicio = responseServicio.Result[0];
                    ViewBag.TituloPagina = responseServicio.Result[0].TituloPagina;
                    ViewBag.Metadata = responseServicio.Result[0].MetaPagina;
                }
                

                //Footer = 
                var responseGeneral = await _apiService.GetDatosGeneral(url, Constants.UrlAPP, "/api/General/GetFullInfo", _dataAccess?.token_type, _dataAccess?.access_token);

                if (responseGeneral.IsSuccess)
                {
                    if (responseGeneral.Result.General.Count > 0)
                    {
                        ViewBag.Footer = responseGeneral.Result.General[0];
                        ViewBag.FooterContacto = responseGeneral.Result.Contacto[0];
                    }
                }
            }
            catch(Exception e)
            {
                var msg = e.Message; 
            }

            return View();
        }
    }
}