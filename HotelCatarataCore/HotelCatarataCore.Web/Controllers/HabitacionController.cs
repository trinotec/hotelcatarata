﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HotelCatarataCore.Web.Helpers;
using HotelCatarataCore.Web.Models;
using HotelCatarataCore.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocIORenderer;
using Syncfusion.OfficeChart;
using Syncfusion.Pdf;

 
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;

namespace HotelCatarataCore.Web.Controllers
{
    public class HabitacionController : Controller
    {
        private readonly IApiService _apiService;
        private static Random random = new Random();

        private string fraseEncriptacion = "C0st4R1c4Ch3ck1n";
        private string algoritmoEncriptacionHASH = "MD5";
        private string valorRGBSalt = "TextoCualquiera";
        private int iteraciones = 22;
        private string vectorInicial = "1234567891234567";
        private int tamanoClave = 128;

        public HabitacionController(IApiService apiService)
        {
            _apiService = apiService;
        }

        public async Task<IActionResult> Index(int id , string FechaIniCheck = null, string fechaFinCheck = null, int huespedes = 2, int children = 0, int revificacionIncial = 0, int nfb =0)
        {
            Response<HabitacionesResponse> HabitacionesSimilares = new Response<HabitacionesResponse>();
            
            DateTime DtFechaini;
            DateTime DtFechaFin;
            if (DateTime.TryParse(FechaIniCheck, out DtFechaini) && DateTime.TryParse(fechaFinCheck, out DtFechaFin))
            {

                ViewBag.DiaIni = DtFechaini.Day;
                ViewBag.MesActFormat = HelpMonth.ObtenerMes(DtFechaini.Month);
                ViewBag.YearIni = DtFechaini.Year;

                ViewBag.DiaFin = DtFechaFin.Day;
                ViewBag.MesActFormatSalida = HelpMonth.ObtenerMes(DtFechaFin.Month);
                ViewBag.YearFin = DtFechaFin.Year;
                ViewBag.Huespedes = huespedes;
                ViewBag.ninos = children;
                ViewBag.FechaIniCheck = FechaIniCheck;
                ViewBag.FechaFinCheck = fechaFinCheck;
            }
            else
            {
                ViewBag.DiaIni = DateTime.Now.Day;
                ViewBag.MesActFormat = HelpMonth.ObtenerMes(DateTime.Now.Month);
                ViewBag.YearIni = DateTime.Now.Year;

                ViewBag.DiaFin = DateTime.Now.AddDays(1).Day;
                ViewBag.MesActFormatSalida = HelpMonth.ObtenerMes(DateTime.Now.AddDays(1).Month);
                ViewBag.YearFin = DateTime.Now.AddDays(1).Year;
                ViewBag.Huespedes = huespedes;
                ViewBag.ninos = 0;
                ViewBag.FechaIniCheck = DateTime.Now.ToString("yyyy-MM-dd");
                ViewBag.FechaFinCheck = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
            }

            ViewBag.CodigoHabitacion = id;
            ViewBag.revificacionIncial = revificacionIncial;
            ViewBag.nuevaFechaBuscar  = nfb;
            ViewBag.Kaptcha = Constants.UrlKaptcha;

            try
            {
                //informacion de Hotel  ------------------------------
                var url = Constants.UrlAPI;
                var idCodeHotel = Constants.IdCodeHotel;
                var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);
             

                if (responseConexionHotel.Result != null)
                {
                    if (responseConexionHotel.Result.Count > 0)
                    {
                        ViewBag.DataHotel = responseConexionHotel.Result[0];
                        ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                        ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                    }
                }


                //informacion de acceso ------------------------------     
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;

                                 
                string controlApi = "/api/HabitacionesTipos/" +  id.ToString("00");
                //informacion de habitacion---------------------------
                var responseContacto = await _apiService.GetHabitacion(url, Constants.UrlAPP, controlApi, _dataAccess?.token_type, _dataAccess?.access_token);

                if(responseContacto.Result != null)
                {
                    ViewBag.Habitacion = responseContacto.Result;
                    ViewBag.TextoEstadia = responseContacto.Result.MinimoNoches > 1 ? "Noches" : "Noche";
                    ViewBag.TextoNinos = responseContacto.Result.CapacidadNinos > 1 ? "Niños" : "Niño";
                }
                 

                //informacion de habitaciones---------------------------
                var requestHabitaciones = new HomeBannersRequest()
                {
                    Take = 4,
                    Skip = 0,
                    SearchText = "",
                    Activo = true,
                    RoomPrice = 0
                };

                var responseHabitaciones = await _apiService.GetHabitaciones(url, Constants.UrlAPP, "/api/HabitacionesTipos", _dataAccess?.token_type, _dataAccess?.access_token, requestHabitaciones);
                if (responseHabitaciones.IsSuccess)
                {                    
                    ViewBag.HabitacionesSimilares = responseHabitaciones.Result.listaHabitaciones.Where(w=> Int32.Parse(w.CodTipoHab) != id).ToList();
                }

                ViewBag.RandomString = RandomString(32);

                //Footer = 
                var responseGeneral = await _apiService.GetDatosGeneral(url, Constants.UrlAPP, "/api/General/GetFullInfo", _dataAccess?.token_type, _dataAccess?.access_token);

                if (responseGeneral.IsSuccess)
                {
                    if (responseGeneral.Result.General.Count > 0)
                    {
                        ViewBag.Footer = responseGeneral.Result.General[0];
                        ViewBag.FooterContacto = responseGeneral.Result.Contacto[0];
                    }
                }

            }
            catch
            {

            }

            return View();
        }


        [HttpPost]
        public async Task<IActionResult> VeificarReservacionHabitacion(ReservacionVerificacion data)
        {
            var  totalHabitacionPrecio = 0.0;
            try
            {
                //informacion de acceso ------------------------------
                var url = Constants.UrlAPI;
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;

                //informacion de habitaciones--------------------------- 

                var requestHabitaciones = new BannersRequest()
                {
                    Take = 500,
                    Skip = 0, // (NumPagina * 3 - 3),
                    FechaInicio = data.FechaInicio,
                    FechaFinal = data.FechaFinal,
                    CantAdultos = data.TotalHuespedes,
                    CantNinos = data.TotalNinos
                };

                var responseHabitaciones = await _apiService.GetRoomsavailable(url, Constants.UrlAPP, "/api/HabitacionesTipos/GetAvailability", _dataAccess.token_type, _dataAccess.access_token, requestHabitaciones);
                if (responseHabitaciones.IsSuccess)
                {
                    var habitacionDisponible = false;
                    foreach (var habitacion in responseHabitaciones.Result.listaHabitaciones)
                    {
                        if(Int32.Parse(habitacion.CodTipoHab) == data.CodHab)
                        {
                            habitacionDisponible = true;
                            totalHabitacionPrecio = habitacion.Precio;                            
                        }
                    }

                    var enviar = new
                    {
                        IsSuccess = true,
                        IsSuccessHabitacion = habitacionDisponible,
                        totalReserva = totalHabitacionPrecio,
                        totalHabDisponibleFecha = responseHabitaciones.Result.listaHabitaciones.Count()
                    };

                    return Json(enviar);
                }
                else
                {
                    return Json(new { IsSuccess = false, Mensaje = responseHabitaciones.Message });
                }

            }
            catch
            {
                return Json(new { IsSuccess = false, Mensaje = "Error al realizar la reservacion" });
            }

            //return Json(new { IsSuccess = false });
        }


        [HttpPost]        
        public async Task<IActionResult> RealizarReserva(DatosReservacion data)
        {

            string codigoHabitacion = (Int32.Parse(data.CodTipoHab)).ToString("00");
            DateTime fecha1 = DateTime.Parse(data.FechaLlegada);
            DateTime fecha2 = DateTime.Parse(data.FechaSalida);
            int diasEstancia = (int)((fecha2 - fecha1).TotalDays);
            var precioNoche = data.TotalReservacion / diasEstancia;

            try
            {
                //informacion de acceso ------------------------------
                var url = Constants.UrlAPI;
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;


                var requestReservar = new RequestReservacion() { 
                  reserva = new Reserva {
                      
                      CodTipoHab = codigoHabitacion,
                      FechaReserva = DateTime.Now.ToString("yyyy-MM-dd"),
                      FechaLlegada = data.FechaLlegada,
                      FechaSalida = data.FechaSalida,
                      NumNoches = diasEstancia,
                      PrecioNoche = precioNoche,                      
                      Nombres = data.Nombres,
                      Apellidos = data.Apellidos,
                      Telefonos = data.Telefonos,
                      CodPais = data.CodPais,
                      CodCiudad = data.provincia,
                      Email = data.Email,
                      Empresa = data.Empresa,
                      Comentarios = data.Comentarios,
                      Adultos = data.huespedes,
                      Ninos = data.Ninos,
                      StatusReserva = "00",
                      MontoTotalReserva = data.TotalReservacion,
                      NumIdentificacion = "",                      
                      MontoPagado = data.TotalReservacion,
                      PagoEfectivo = 0.0000,
                      PagoTarjeta = data.TotalReservacion,
                      PagoTransferencia = 0.0000,                      
                      NumTransferencia = null,
                  },
                   bin = data.bin,
                   last4 = data.last4 
                };

                var responseReservacion = await _apiService.PostReservacion(url, Constants.UrlAPP, "/api/Reservas/InsertarReserva", _dataAccess.token_type, _dataAccess.access_token, requestReservar);

                if (responseReservacion.IsSuccess)
                {
                    EncriptadorAEScs Seguridad = new EncriptadorAEScs();
                    var Moneda = "USD";
                    var MonedaEncriptadaTemp = Seguridad.cifrarTextoAES(Moneda, fraseEncriptacion, valorRGBSalt, algoritmoEncriptacionHASH, iteraciones, vectorInicial, tamanoClave);
                    var OrdenReferenciaEncriptadoTemp = Seguridad.cifrarTextoAES(responseReservacion.Result.orderReference.ToString(), fraseEncriptacion, valorRGBSalt, algoritmoEncriptacionHASH, iteraciones, vectorInicial, tamanoClave);
                    var MontoEncriptadoTemp = Seguridad.cifrarTextoAES(data.TotalReservacion.ToString(), fraseEncriptacion, valorRGBSalt, algoritmoEncriptacionHASH, iteraciones, vectorInicial, tamanoClave);

                    return Json(new { IsSuccess = true, responseReservacion.Result , dataEnc = new { MonedaEncriptada = MonedaEncriptadaTemp, OrdenReferenciaEncriptado = OrdenReferenciaEncriptadoTemp, MontoEncriptado = MontoEncriptadoTemp } });
                }
                else
                {
                    return Json(new { IsSuccess = false, Error = responseReservacion.Message });
                }
            }
            catch(Exception e)
            {
                return Json(new { IsSuccess = false, Error = e.Message });
            }
        }


        public static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
               .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        [HttpPost]
        public IActionResult EncrDato(string valor,bool stringBool,bool cleanSpace)
        {
            
            //stringBool Verifico si es un valor string en caso contrario convierto a entero.
            //----------------
            EncriptadorAEScs Seguridad = new EncriptadorAEScs();
            if (stringBool != true)
            {
                var ValorEncriptar = Int32.Parse(valor);
                var DatoEncriptado = Seguridad.cifrarTextoAES(ValorEncriptar.ToString(), fraseEncriptacion, valorRGBSalt, algoritmoEncriptacionHASH, iteraciones, vectorInicial, tamanoClave);
                return Json(new { valor = DatoEncriptado });
            }
            else
            {

                if (cleanSpace == true)
                {
                     valor = valor.Replace(" ", string.Empty);
                }
                var DatoEncriptado = Seguridad.cifrarTextoAES(valor, fraseEncriptacion, valorRGBSalt, algoritmoEncriptacionHASH, iteraciones, vectorInicial, tamanoClave);
                return Json(new { valor = DatoEncriptado });
            } 
        }


        [HttpPost]
        public async Task<IActionResult> PagarReserva(RequestPago data)
        {
 
            try
            {
                //informacion de acceso ------------------------------
                var url = Constants.UrlAPI;
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;
                data.Session = string.Empty;

                var responseReservacion = await _apiService.PostPagarReserva(url, Constants.UrlAPP, "/api/Reservas/Pagar", _dataAccess.token_type, _dataAccess.access_token, data);
 
                if (responseReservacion.IsSuccess)
                {
                    return Json(responseReservacion);
                }
                else
                {
                    return Json(new { IsSuccess = "False", Error = responseReservacion.Message });
                }

            }
            catch (Exception e)
            {
                return Json(new { IsSuccess = false, Error = e.Message });
            }

        }

 
        public async Task<IActionResult> DownloadAttachment(string idReserva)
        {

            //informacion de acceso ------------------------------
            var url = Constants.UrlAPI;
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;
            //-----------------------------------------------------
            var responseReservacion = await _apiService.GetReservacion(url, Constants.UrlAPP, "/api/Reservas/Consultar", _dataAccess.token_type, _dataAccess.access_token, idReserva);            

            if (!responseReservacion.IsSuccess)
            {
                //podria redireccionar a una pagina donde diga que no se encontro       
                return RedirectToAction("DescargaReseva");
            }
            var reserva = responseReservacion.Result.reservasDetalle;
            CultureInfo.CurrentCulture = CultureInfo.CreateSpecificCulture("es-CR");

            var fechaIniTemp = DateTime.Parse(reserva.FechaLlegada, CultureInfo.CurrentCulture);
            var fechaFinTemp = DateTime.Parse(reserva.FechaSalida, CultureInfo.CurrentCulture);


            //-----------------------------------------------------

            var urlLogo = responseReservacion.Result.URLLogo.URLLogoFullPath;             
            var urlImgHotel = responseReservacion.Result.URLLogo.URLHabitacionesFullPath;
            var NombreCliente = reserva.Nombres;
            var NumReserva = reserva.CodReserva.ToString();
            var fechaIniR = fechaIniTemp.ToString("dd") + " de " + fechaIniTemp.ToString("MMMM");
            var fechaFinR = fechaFinTemp.ToString("dd") + " de " + fechaIniTemp.ToString("MMMM");
            var EmailHotel = responseReservacion.Result.Hotel.Correo;
            var TelefonoHotel = responseReservacion.Result.Hotel.Telefono;
            var DireccionHotel = responseReservacion.Result.Hotel.Direccion;
            var fechaIniR2 = fechaIniTemp.ToString("dd/MM/yyyy");
            var fechaRinR2 = fechaFinTemp.ToString("dd/MM/yyyy");
            var nombreClienteCompleto = reserva.Nombres + " " + reserva.Apellidos;
            var TotalHuesped = (reserva.Adultos + reserva.Ninos).ToString();
            var ValorReserva = string.Format("{0:c}", reserva.MontoTotalReserva);

            FileStream fileStreamPath = new FileStream(@"wwwroot\TemplateWord\PlantillaReservacion.docx", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            WordDocument document = new WordDocument(fileStreamPath, FormatType.Docx);
            

            document.MailMerge.MergeImageField += new MergeImageFieldEventHandler(MergeField_ProductImage);
            
            string[] fieldNames = new string[] { "ImageLogo", 
                "NombreCliente", 
                "NumReserva" ,
                "FechaIniReserva",
                "FechaFinReserva",
                "CorreoHotel",
                "TelefonoContacto",
                "ImageHotel",
                "DireccionHotel",
                "FechaIniReserva2",
                "FechaFinReserva2",
                "NombreClienteCompleto",
                "TotalHuesped",
                "ValorReserva"

            };

            string[] fieldValues = new string[] { urlLogo ,
                NombreCliente,
                NumReserva,
                fechaIniR,
                fechaFinR,
                EmailHotel,
                TelefonoHotel,
                urlImgHotel,
                DireccionHotel,
                fechaIniR2,
                fechaRinR2,
                nombreClienteCompleto,
                TotalHuesped,
                ValorReserva

            };

            document.MailMerge.Execute(fieldNames, fieldValues); 
            DocIORenderer render = new DocIORenderer();
            render.Settings.ChartRenderingOptions.ImageFormat = ExportImageFormat.Jpeg;
            PdfDocument pdfDocument = render.ConvertToPDF(document);
            render.Dispose();
            document.Dispose();
            MemoryStream outputStream = new MemoryStream();
            pdfDocument.Save(outputStream);
            pdfDocument.Close();
             
            outputStream.Position = 0;

            if(controlLog == true)
            {
                imageStreamLogo.Dispose();
                pictureLogo.Dispose();
                imageLogo.Dispose();
            }
            
            if(controImg == true)
            {
                imageStreamImg.Dispose();
                pictureImg.Dispose();
                imageImg.Dispose();
            }

            //Download Word document in the browser
            return File(outputStream, "application/pdf", "Reservacion.pdf");
        }


        bool controlLog;
        FileStream imageStreamLogo;
        WPicture pictureLogo; 
        Bitmap imageLogo;

        bool controImg;
        FileStream imageStreamImg;
        WPicture pictureImg;
        Bitmap imageImg;
        private void MergeField_ProductImage(object sender, MergeImageFieldEventArgs args)
        {
            
            if (args.FieldName == "ImageLogo")
            {
                controlLog = false;
                string uri = args.FieldValue.ToString();
                string filename = "ejemplo.jpg";
                string localFolder = @"wwwroot\TemplateWord\";


                System.IO.File.Delete(@"wwwroot\TemplateWord\ejemplo.jpg");


                HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(Path.Combine(uri));
                try
                {                  
                    using (HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse())
                    {
                        using (Stream responseStream = httpResponse.GetResponseStream())
                        {
                            using (FileStream localFileStream =
                                new FileStream(Path.Combine(localFolder, filename), FileMode.Create))
                            {
                                var buffer = new byte[4096];
                                long totalBytesRead = 0;
                                int bytesRead;

                                while ((bytesRead = responseStream.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    totalBytesRead += bytesRead;
                                    localFileStream.Write(buffer, 0, bytesRead);
                                }                                
                            }                            
                        }
                    }

                    //System.IO.File.Delete(@"wwwroot\TemplateWord\ejemplo.jpg");

                    string ProductFileName = args.FieldValue.ToString();
                    string pathImage = @"wwwroot\TemplateWord\ejemplo.jpg";

                    //FileStream imageStream = new FileStream(pathImage, FileMode.Open, FileAccess.Read);
                    imageStreamLogo = new FileStream(pathImage, FileMode.Open, FileAccess.Read);
                    args.ImageStream = imageStreamLogo;
                    //WPicture picture = args.Picture;
                    pictureLogo = args.Picture;
                    //var image = new Bitmap(imageStream);
                    imageLogo = new Bitmap(imageStreamLogo);
                    double Anchotemp = imageLogo.Width / (imageLogo.Height / 30.0);
                    int ancho = (int)Math.Ceiling(Anchotemp);

                    //Resizes the picture
                    pictureLogo.Height = 30;
                    pictureLogo.Width = ancho;

                    controlLog = true;

                }
                catch (Exception ex)
                {
                     
                }
            }

            //Binds image from file system during mail merge
            if (args.FieldName == "ImageHotel")
            {
                controImg = false;
                string uri = args.FieldValue.ToString();
                string filename = "imgHotel.jpg";
                string localFolder = @"wwwroot\TemplateWord\";

                HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(Path.Combine(uri));
                httpRequest.Method = "GET";
                try
                {
                    
                    using (HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse())
                    {
                        using (Stream responseStream = httpResponse.GetResponseStream())
                        {
                            using (FileStream localFileStream =
                                new FileStream(Path.Combine(localFolder, filename), FileMode.Create))
                            {
                                var buffer = new byte[4096];
                                long totalBytesRead = 0;
                                int bytesRead;

                                while ((bytesRead = responseStream.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    totalBytesRead += bytesRead;
                                    localFileStream.Write(buffer, 0, bytesRead);
                                }
                            }
                        }
                    }


                    string ProductFileName = args.FieldValue.ToString();
                    string pathImage = @"wwwroot\TemplateWord\imgHotel.jpg";
                    imageStreamImg = new FileStream(pathImage, FileMode.Open, FileAccess.Read);
                    args.ImageStream = imageStreamImg;
                    pictureImg = args.Picture;

                    imageImg = new Bitmap(imageStreamImg);
                    double div = imageImg.Width / 320.0;
                    double Altotemp = imageImg.Height / div;
                    int Alto = (int)Math.Ceiling(Altotemp);

                    pictureImg.Height = Alto;
                    pictureImg.Width = 320;
                    controImg = true;



                }
                catch (Exception ex)
                {

                }
            }

        }


        public async Task<IActionResult> DescargaReseva()
        {
            //informacion de Hotel  ------------------------------
            var url = Constants.UrlAPI;
            var idCodeHotel = Constants.IdCodeHotel;
            var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);


            if (responseConexionHotel.Result != null)
            {
                if (responseConexionHotel.Result.Count > 0)
                {
                    ViewBag.DataHotel = responseConexionHotel.Result[0];
                    ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                    ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                }
            }


            //informacion de acceso ------------------------------     
            var acceso = GetAccess.Instance(_apiService).GetToken();
            var _dataAccess = acceso.Result;
            return View();
        }

    }
}