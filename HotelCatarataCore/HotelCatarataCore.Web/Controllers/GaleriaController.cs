﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelCatarataCore.Web.Helpers;
using HotelCatarataCore.Web.Models;
using HotelCatarataCore.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HotelCatarataCore.Web.Controllers
{
    public class GaleriaController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IApiService _apiService;

        public GaleriaController(ILogger<HomeController> logger, IApiService apiService)
        {
            _logger = logger;
            _apiService = apiService;
        }

        public async Task<IActionResult> Index()
        {
            
            try
            {

                //informacion de Hotel  ------------------------------
                var url = Constants.UrlAPI;
                var idCodeHotel = Constants.IdCodeHotel;
                var responseConexionHotel = await _apiService.GetConexionHotel(url, Constants.UrlAPP, "/api/Login/Conectar", idCodeHotel);

                if (responseConexionHotel.Result != null)
                {
                    if (responseConexionHotel.Result.Count > 0)
                    {
                        ViewBag.DataHotel = responseConexionHotel.Result[0];
                        ViewBag.TituloPagina = responseConexionHotel.Result[0].TituloPagina;
                        ViewBag.Metadata = responseConexionHotel.Result[0].MetaPagina;
                    }
                }


                //informacion de acceso ------------------------------     
                var acceso = GetAccess.Instance(_apiService).GetToken();
                var _dataAccess = acceso.Result;

                //informacion Tab's---------------------------
                var responseTabs = await _apiService.GetTabsGaleria(url, Constants.UrlAPP, "/api/Galeria/GetTabNames", _dataAccess?.token_type, _dataAccess?.access_token);

                var TabsMenu = new List<TabGaleria>();
                
                if (responseTabs.Result != null)
                {
                    TabsMenu = responseTabs.Result.Where(x => x.Tab != 0).ToList();
                    var FirtsTAb = TabsMenu.First();
                    FirtsTAb.Active = "active";
                    FirtsTAb.AriaSelected = "true";
                    FirtsTAb.ClassTabContent = "show active";
                }

                //informacion Galeria---------------------------
                var responseGaleria = await _apiService.GetGaleria(url, Constants.UrlAPP, "/api/Galeria", _dataAccess?.token_type, _dataAccess?.access_token);

                if(responseGaleria.Result != null && TabsMenu.Count > 0)
                {
                    foreach (var item in TabsMenu)
                    {
                        item.Galeria = responseGaleria.Result.Where(x => x.Tab == item.Tab).ToList();
                    }
                    ViewBag.GaleriaImagenes = TabsMenu;
                    ViewBag.TituloPagina = responseGaleria.Result[0].TituloPagina;
                    ViewBag.Metadata = responseGaleria.Result[0].MetaPagina;
                }


                //Seleccionaod el tap del header
                if (responseTabs.Result != null)
                {
                    var TabHeader = responseTabs.Result.Where(x=>x.Tab==0).FirstOrDefault();
                    TabHeader.Galeria = responseGaleria.Result.Where(x => x.Tab == TabHeader.Tab).ToList();
                    ViewBag.HeaderGaleria = (TabHeader.Galeria.Count>0) ? TabHeader.Galeria[0].ImageFullPath:"xx"; 
                }

                //Footer = 
                var responseGeneral = await _apiService.GetDatosGeneral(url, Constants.UrlAPP, "/api/General/GetFullInfo", _dataAccess?.token_type, _dataAccess?.access_token);
                if (responseGeneral.IsSuccess)
                {
                    if (responseGeneral.Result.General.Count > 0)
                    {
                        ViewBag.Footer = responseGeneral.Result.General[0];
                        ViewBag.FooterContacto = responseGeneral.Result.Contacto[0];
                    }
                }

            }
            catch
            {

            }

            //return View();
            return await Task.Run(() => View());
        }
    }
}