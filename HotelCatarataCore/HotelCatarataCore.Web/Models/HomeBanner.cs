﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class HomeBanner
    {
        public int Id { get; set; }
        public string Texto1 { get; set; }
        public string Texto2 { get; set; }
        public string Imagen { get; set; }
        public string Imagen1 { get; set; }
        public string Imagen2 { get; set; }
        public object Imagen3 { get; set; }
        public string Link { get; set; }
        public bool Activo { get; set; }
        public string ImageFullPath { get; set; }
        public string Image1FullPath { get; set; }
        public string Image2FullPath { get; set; }
        public string Image3FullPath { get; set; }
        public object ImagenBase64 { get; set; }
        public object Imagen1Base64 { get; set; }
        public object Imagen2Base64 { get; set; }
        public object Imagen3Base64 { get; set; }
    }
}
