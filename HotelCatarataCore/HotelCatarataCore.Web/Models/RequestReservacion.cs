﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class RequestReservacion
    {
 
        public Reserva reserva { get; set; }        
        public string bin { get; set; }
        public string last4 { get; set; }
    }
}
