﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class ReservacionVerificacion
    {
        public int CodHab { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFinal { get; set; }
        public int TotalHuespedes { get; set; }
        public int TotalNinos { get; set; }
    }
}
