﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class Nosotros
    {
        public int Id { get; set; }
        public string ImagenPrincipal { get; set; }
        public string ImagenSecundaria1 { get; set; }
        public string ImagenSecundaria2 { get; set; }
        public string ImagenSecundaria3 { get; set; }
        public string ImagenSecundaria4 { get; set; }
        public string Titulo { get; set; }
        public string Texto { get; set; }
        public string ImagenFondoVideo { get; set; }
        public string UrlVideo { get; set; }
        public string ImagenPrincipalFullPath { get; set; }
        public string ImagenSecundaria1FullPath { get; set; }
        public string ImagenSecundaria2FullPath { get; set; }
        public string ImagenSecundaria3FullPath { get; set; }
        public string ImagenSecundaria4FullPath { get; set; }
        public string ImagenFondoVideoFullPath { get; set; }
        public string Titulo1Video { get; set; }
        public string Titulo2Video { get; set; }

        public object TituloPagina { get; set; }
        public object MetaPagina { get; set; }
    }
}
