﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class TabGaleria
    {
        public int Tab { get; set; }
        public string TabName { get; set; }
        public List<Galeria> Galeria { get; set; }

        public string Active { get; set; } = "";
        public string AriaSelected { get; set; } = "false";        
        public string ClassTabContent { get; set; } = "";
    }
}
