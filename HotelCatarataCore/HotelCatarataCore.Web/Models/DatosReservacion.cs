﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class DatosReservacion
    {
        public string CodTipoHab { get; set; }
        public string CodReserva { get; set; }
        public string FechaLlegada { get; set; }
        public string FechaSalida { get; set; }
        public int huespedes { get; set; }
        public int Ninos { get; set; }
        public double TotalReservacion { get; set; }
        public string TotalReservacionEnc { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Telefonos { get; set; }
        public string Email { get; set; }
        public string Empresa { get; set; }
        public string Comentarios { get; set; }
     

        public string CedulaFactura { get; set; }
        public string CodigoPostal { get; set; }
        public string DireccionFactura { get; set; }
        public string DireccionUsuario { get; set; }        
        public string EmailFactura { get; set; }
        public string NombreFactura { get; set; }        
        public string PhoneFactura { get; set; }
        public string chkFactura { get; set; }
        public string provincia { get; set; }
        public string TipoIdentificacion { get; set; }

        public string AutorizacionTarjeta { get; set; }
        public string ReferenciaTarjeta { get; set; }
        public string MesTarjeta { get; set; }
        public string AnyoTarjeta { get; set; }
        public string CodPais { get; set; }
        public string TitularTarjeta { get; set; }
        public string StringSesion { get; set; }
        [JsonProperty(PropertyName = "kountSession")]
        public string kountSession { get; set; }
        public string Moneda { get; set; }

        public string bin { get; set; }
        public string last4 { get; set; }

    }
}
