﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class Galeria
    {
        public int Id { get; set; }
        public int Tab { get; set; }
        public string TabName { get; set; }
        public string ImageTitle { get; set; }
        public string Image { get; set; }
        public bool Active { get; set; }
        public string ImageFullPath { get; set; }
        public object ImageBase64 { get; set; }
        public object TituloPagina { get; set; }
        public object MetaPagina { get; set; }
    }
}
