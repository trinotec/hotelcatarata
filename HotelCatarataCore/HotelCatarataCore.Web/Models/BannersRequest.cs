﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class BannersRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFinal { get; set; }
        public int CantAdultos { get; set; }
        public int CantNinos { get; set; }

    }
}
