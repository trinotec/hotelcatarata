﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class PrecioHabitacion
    {
        public Decimal Precio { get; set; }
    }
}
