﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class DatosGeneral
    {
        public int id { get; set; }
        public string URLLogo { get; set; }
        public string URLLogoFullPath { get; set; }
        public object URLLogoB64 { get; set; }
        public string URLFace { get; set; }
        public string URLInstagram { get; set; }
        public object URLTwiter { get; set; }
        public object URLPinterest { get; set; }
        public string Email { get; set; }
        public string HoraCheckin { get; set; }
        public string HoraCheckout { get; set; }
        public string TerminosyCondiciones { get; set; }
        public string PoliticasPrivacidad { get; set; }
        public string PoliticaCancelacion { get; set; }
        public string Color1 { get; set; }
        public string Color2 { get; set; }      
        public object URLHabitaciones { get; set; }
        public object URLHabitacionesFullPath { get; set; }
        public object URLHabitacionesB64 { get; set; }
                          
    }
}
