﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class Amenidad
    {
        public string CodAmenidad { get; set; }
        public string NomAmenidad { get; set; }
    }
}
