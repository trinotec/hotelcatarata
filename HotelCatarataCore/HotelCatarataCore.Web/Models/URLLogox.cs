﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    //Clase utilizada para obtener datos para el pdf de la reserva
    public class URLLogox
    {
        public int id { get; set; }
        public string URLLogo { get; set; }
        public string URLLogoFullPath { get; set; }
        public string URLHabitaciones { get; set; }
        public string URLHabitacionesFullPath { get; set; }
        public string URLHabitacionesB64 { get; set; }
        public string URLLogoB64 { get; set; }
        public string FavIconB64 { get; set; }
        public string URLFace { get; set; }
        public string URLInstagram { get; set; }
        public string URLTwiter { get; set; }
        public string URLPinterest { get; set; }
        public string Email { get; set; }
        public string HoraCheckin { get; set; }
        public string HoraCheckout { get; set; }
        public string TerminosyCondiciones { get; set; }
        public string PoliticasPrivacidad { get; set; }
        public string PoliticaCancelacion { get; set; }
        public string Color1 { get; set; }
        public string Color2 { get; set; }
        public string TituloPagina { get; set; }
        public string MetaPagina { get; set; }
        public string FavIcon { get; set; }
        public string FavIconFullPath { get; set; }
    }
}
