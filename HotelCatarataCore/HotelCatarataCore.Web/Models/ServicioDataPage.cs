﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class ServicioDataPage
    {
        public int Id { get; set; }
        public string ImagenPrincipal { get; set; }
        public string ImagenPrincipalFullPath { get; set; }
        public object ImagenPrincipalBase64 { get; set; }
        public string Imagen1 { get; set; }
        public string Titulo1 { get; set; }
        public string Imagen2 { get; set; }
        public string Titulo2 { get; set; }
        public string Imagen3 { get; set; }
        public string Titulo3 { get; set; }
        public string Imagen1FullPath { get; set; }
        public object Imagen1Base64 { get; set; }
        public string Imagen2FullPath { get; set; }
        public object Imagen2Base64 { get; set; }
        public string Imagen3FullPath { get; set; }
        public object Imagen3Base64 { get; set; }

        public object TituloPagina { get; set; }
        public object MetaPagina { get; set; }
    }
}
