﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class DataFullInfo
    {
        public List<DatosGeneral> General { get; set; }
        public List<Contacto> Contacto { get; set; }
    }
}
