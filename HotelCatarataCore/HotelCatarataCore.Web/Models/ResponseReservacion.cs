﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class ResponseReservacion
    {
        public string CodReserva { get; set; }
        public int orderReference { get; set; }
        public OrderResponse OrderResponse { get; set; }
    }
}
