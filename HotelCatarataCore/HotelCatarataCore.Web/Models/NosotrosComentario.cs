﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class NosotrosComentario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Puesto { get; set; }
        public string DescripcionComentario { get; set; }
        public object Foto { get; set; }
        public bool Activo { get; set; }
        public string FotoFullPath { get; set; }
        public object FotoBase64 { get; set; }
    }
}
