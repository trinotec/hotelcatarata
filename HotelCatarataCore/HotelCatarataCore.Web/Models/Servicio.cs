﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class Servicio
    {
        public string CodServicio { get; set; }
        public string NomServicio { get; set; }
        public string Icono { get; set; }
        public string IconoFullPath { get; set; }
    }
}
