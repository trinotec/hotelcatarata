﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class HabitacionesResponse
    {
        public int XTotalCount { get; set; }
        public List<Habitacion> listaHabitaciones { get; set; }
    }
}
