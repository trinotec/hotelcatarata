﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class HomeBannersRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SearchText { get; set; }
        public bool Activo { get; set; }
        public double RoomPrice { get; set; }
    }
}
