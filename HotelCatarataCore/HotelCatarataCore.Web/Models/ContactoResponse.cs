﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class ContactoResponse
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public string Asunto { get; set; }
        public string Mensaje { get; set; }
        public string Email { get; set; }
        public string NomCliente { get; set; }
        public bool Enviado { get; set; }
    }
}
