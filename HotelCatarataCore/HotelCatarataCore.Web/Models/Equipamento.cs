﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class Equipamento
    {
        public string CodEquipamento { get; set; }
        public string NomEquipamento { get; set; }
    }
}
