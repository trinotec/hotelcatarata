﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class HomeBannersSecundario
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public object Link { get; set; }
        public bool Activo { get; set; }
        public string Imagen1 { get; set; }
        public string Imagen2 { get; set; }
        public object Imagen3 { get; set; }
        public object Imagen4 { get; set; }
        public string Image1FullPath { get; set; }
        public string Image2FullPath { get; set; }
        public string Image3FullPath { get; set; }
        public string Image4FullPath { get; set; }
    }
}
