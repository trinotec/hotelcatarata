﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class ResponseConsultaReserva
    {
        public Reserva reservasDetalle { get; set; }
        public HotelDatoReserva Hotel { get; set; }
        public URLLogox URLLogo { get; set; }
    }
}
