﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class HotelData
    {
        public string TituloPagina { get; set; }
        public string MetaPagina { get; set; }
        public string FavIconFullPath { get; set; }
    }
}
