﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class HomeServices
    {
        public int Id { get; set; }
        public string NomServicio { get; set; }
        public string Descripcion { get; set; }
        public object Link { get; set; }
        public bool Activo { get; set; }
        public string Imagen1 { get; set; }
        public string Image1FullPath { get; set; }
        public int NumClass { get; set; }
    }
}
