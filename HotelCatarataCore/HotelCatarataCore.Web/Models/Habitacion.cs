﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class Habitacion
    {
        
        public List<Foto> Fotos { get; set; }        
        public List<Servicio> Servicios { get; set; }
        public string CodTipoHab { get; set; }
        public string NombreTipoHab { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionDetallada { get; set; }
        public string Camas { get; set; }
        public int CapacidadAdultos { get; set; }
        public int CapacidadNinos { get; set; }
        public int CapacidadMin { get; set; }
        public int MinimoNoches { get; set; }
        public int TotalHabitaciones { get; set; }
        public double Precio { get; set; }
        public object TipoPrecio { get; set; }
        public List<Amenidad> Amenidades { get; set; }
        public int CantidadBanos { get; set; }
        public int TamanoHabitacion { get; set; }
        public string CodHotel { get; set; }

        //public List<Equipamento> Equipamentos { get; set; }
        //public List<object> Ofertas { get; set; } //se descomentara cuando se sepa las propieadades de Ofertas y se requiera poner en html
        //public List<Accesibilidad> Accesibilidades { get; set; }        

    }
}
