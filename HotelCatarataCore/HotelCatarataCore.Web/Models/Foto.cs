﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class Foto
    {
        public int IdFoto { get; set; }
        public string CodTipo { get; set; }
        public string Imagen { get; set; }
        public object ImagenB64 { get; set; }
    }
}
