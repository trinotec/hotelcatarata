﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class RequestPago
    {
        public string OrderReference { get; set; }
        public string CodReserva { get; set; }
        public string Monto { get; set; }
        public string Moneda { get; set; }
        public string kountSession { get; set; }
        public string Session { get; set; }
        public string LD { get; set; }
        public string LK { get; set; }
    }
}
