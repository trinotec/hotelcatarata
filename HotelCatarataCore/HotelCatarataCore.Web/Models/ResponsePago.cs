﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class ResponsePago
    {
        public bool success { get; set; }
        public string message { get; set; }
        public Reserva reserva { get; set; }
    }
}
