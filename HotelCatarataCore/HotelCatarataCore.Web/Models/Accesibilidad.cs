﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class Accesibilidad
    {
        public string CodAccesibilidad { get; set; }
        public string NomAccesibilidad { get; set; }
    }
}
