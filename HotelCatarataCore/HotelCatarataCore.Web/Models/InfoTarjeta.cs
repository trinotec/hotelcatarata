﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class InfoTarjeta
    {
        public string Titular { get; set; }
        public string NumTarjeta { get; set; }
        public string CVC { get; set; }
        public string Mes { get; set; }
        public string Ano { get; set; }
        public string Monto { get; set; }
        public string Moneda { get; set; }
    }
}
