﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class Contacto
    {
        public int Id { get; set; }
        public string Direccion { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Email { get; set; }
        public string Nombre { get; set; }
        public string CoordenadasLatitud { get; set; }
        public string CoordenadasLongitud { get; set; }
        public string Image { get; set; }
        public string ImageFullPath { get; set; }
        public object ImageBase64 { get; set; }
        public object TituloPagina { get; set; }
        public object MetaPagina { get; set; }
    }
}
