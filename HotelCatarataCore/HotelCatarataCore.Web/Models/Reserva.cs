﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class Reserva
    {
        public string CodReserva { get; set; }
        public string CodHotel { get; set; }
        public string CodTipoHab { get; set; }
        public string FechaReserva { get; set; }
        public string FechaLlegada { get; set; }
        public string FechaSalida { get; set; }
        public int NumNoches { get; set; }
        public double PrecioNoche { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Telefonos { get; set; }
        public string NumIdentificacion { get; set; }
        public string CodPais { get; set; }
        public string CodCiudad { get; set; }
        public string Email { get; set; }
        public string Empresa { get; set; }
        public string Comentarios { get; set; }
        public int Adultos { get; set; }
        public int Ninos { get; set; }
        public string StatusReserva { get; set; }
        public double MontoTotalReserva { get; set; }
        public double MontoPagado { get; set; }
        public double PagoEfectivo { get; set; }
        public double PagoTarjeta { get; set; }
        public double PagoTransferencia { get; set; }
        public string NumTransferencia { get; set; }
        public string MotivoCancelacion { get; set; }
        public string MotivoFecha { get; set; }

        //public object NumeroComprobantePago { get; set; }

    }
}
