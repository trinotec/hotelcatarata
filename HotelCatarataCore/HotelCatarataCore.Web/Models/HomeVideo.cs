﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Models
{
    public class HomeVideo
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Url { get; set; }
        public string ImagenFondo { get; set; }
        public bool Activo { get; set; }
        public string ImagenFondoFullPath { get; set; }
        public object ImagenFondoBase64 { get; set; }
    }
}
