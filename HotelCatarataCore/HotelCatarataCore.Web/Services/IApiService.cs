﻿using HotelCatarataCore.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Services
{
    public interface IApiService
    {
        Task<Response<Access>> GetAccess(
            string urlBase,
            string servicePrefix,
            string controller,
            AccessRequest request
        );


        Task<Response<List<HomeBanner>>> GetHomeBanners(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            HomeBannersRequest request);

        Task<Response<List<HomeBannersSecundario>>> GetHomeBannersSecundario(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            HomeBannersRequest request);

        Task<Response<List<HomeServices>>> GetHomeServices(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            HomeBannersRequest request);


        Task<Response<HabitacionesResponse>> GetHabitaciones(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            HomeBannersRequest request);

        Task<Response<List<HomeVideo>>> GetHomeVideo(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            HomeBannersRequest request);

        Task<Response<List<Nosotros>>> GetNosotros(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            HomeBannersRequest request);

        Task<Response<List<NosotrosComentario>>> GetNosotrosComentarios(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            HomeBannersRequest request);


        Task<Response<List<PrecioHabitacion>>> GetRoomPrice(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken
            );

        Task<Response<List<Contacto>>> GetContact(
           string urlBase,
           string servicePrefix,
           string controller,
           string tokenType,
           string accessToken
           );

        Task<Response<ContactoResponse>> PostContactoMensaje(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            MensajeContacto request);

        Task<Response<List<TabGaleria>>> GetTabsGaleria(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken
            );

        Task<Response<List<Galeria>>> GetGaleria(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken
            );

        Task<Response<Habitacion>> GetHabitacion(
           string urlBase,
           string servicePrefix,
           string controller,
           string tokenType,
           string accessToken
           );

        Task<Response<HabitacionesResponse>> GetRoomsavailable(
           string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken, 
            BannersRequest request);

        Task<Response<ResponseReservacion>> PostReservacion(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            RequestReservacion request);

        Task<Response<ResponsePago>> PostPagarReserva(
            string urlBase,
            string servicePrefix,
            string controller,
            string tokenType,
            string accessToken,
            RequestPago request);


        Task<Response<DataFullInfo>> GetDatosGeneral(
               string urlBase,
               string servicePrefix,
               string controller,
               string tokenType,
               string accessToken
               );

        Task<Response<List<ServicioDataPage>>> GetDatosServicios(
               string urlBase,
               string servicePrefix,
               string controller,
               string tokenType,
               string accessToken
               );

        Task<Response<List<HotelData>>> GetConexionHotel(
                string urlBase,
                string servicePrefix,
                string controller,
                string idCodeHotel 
               );

        Task<Response<ResponseConsultaReserva>> GetReservacion(
               string urlBase,
               string servicePrefix,
               string controller,
               string tokenType,
               string accessToken,
               string IdReserva
               );
    }

}
