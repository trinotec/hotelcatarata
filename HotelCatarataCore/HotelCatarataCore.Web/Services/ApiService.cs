﻿using HotelCatarataCore.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Services
{
    public class ApiService: IApiService
    {
        public async Task<Response<Access>> GetAccess(string urlBase, string servicePrefix, string controller, AccessRequest request)
        {
            try
            {

                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(
                    string.Format("grant_type=password&username={0}&password={1}", request.UserName, request.Password),
                    Encoding.UTF8,
                    "application/x-www-form-urlencoded"
                    );

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";

                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<Access>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result),
                    };
                }

                var ResultAccess = JsonConvert.DeserializeObject<Access>(result);
                return new Response<Access>
                {
                    IsSuccess = true,
                    Result = ResultAccess
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<Access>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }


        public async Task<Response<List<HomeBanner>>> GetHomeBanners(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, HomeBannersRequest request)
        {
            try
            {
                //var data = new EvaluacionRequest { estatus = 2 };
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("Take={0}&Skip={1}&SearchText={2}&Activo={3}", request.Take, request.Skip,request.SearchText, request.Activo);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<HomeBanner>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<HomeBanner>>(result);
                return new Response<List<HomeBanner>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<HomeBanner>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<List<HomeBannersSecundario>>> GetHomeBannersSecundario(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, HomeBannersRequest request)
        {
            try
            {
                //var data = new EvaluacionRequest { estatus = 2 };
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("Take={0}&Skip={1}&SearchText={2}&Activo={3}", request.Take, request.Skip, request.SearchText, request.Activo);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<HomeBannersSecundario>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<HomeBannersSecundario>>(result);
                return new Response<List<HomeBannersSecundario>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<HomeBannersSecundario>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<List<HomeServices>>> GetHomeServices(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, HomeBannersRequest request)
        {
            try
            {
                //var data = new EvaluacionRequest { estatus = 2 };
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("Take={0}&Skip={1}&SearchText={2}&Activo={3}", request.Take, request.Skip, request.SearchText, request.Activo);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<HomeServices>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<HomeServices>>(result);
                return new Response<List<HomeServices>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<HomeServices>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }


        public async Task<Response<HabitacionesResponse>> GetHabitaciones(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, HomeBannersRequest request)
        {
            try
            {
                //var data = new EvaluacionRequest { estatus = 2 };
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                string TotalHabitaciones="0";
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("Take={0}&Skip={1}&SearchText={2}&Activo={3}&RoomPrice={4}", request.Take, request.Skip, request.SearchText, request.Activo,request.RoomPrice);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<HabitacionesResponse>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<Habitacion>>(result);

                if (response.Headers.Contains("X-Total-Count"))
                {
                    TotalHabitaciones = response.Headers.GetValues("X-Total-Count").First();
                }

                HabitacionesResponse Habitaciones = new HabitacionesResponse()
                {
                    listaHabitaciones = list,
                    XTotalCount = Int32.Parse(TotalHabitaciones)
                };



                return new Response<HabitacionesResponse>
                {
                    IsSuccess = true,
                    Result = Habitaciones
                };
            }
            catch (Exception ex)
            {
                return new Response<HabitacionesResponse>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<List<HomeVideo>>> GetHomeVideo(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, HomeBannersRequest request)
        {
            try
            {
                //var data = new EvaluacionRequest { estatus = 2 };
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("Take={0}&Skip={1}&SearchText={2}&Activo={3}", request.Take, request.Skip, request.SearchText, request.Activo);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<HomeVideo>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<HomeVideo>>(result);
                return new Response<List<HomeVideo>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<HomeVideo>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<List<Nosotros>>> GetNosotros(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, HomeBannersRequest request)
        {
            try
            {                
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("Take={0}&Skip={1}&SearchText={2}&Activo={3}", request.Take, request.Skip, request.SearchText, request.Activo);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<Nosotros>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<Nosotros>>(result);
                return new Response<List<Nosotros>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<Nosotros>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<List<NosotrosComentario>>> GetNosotrosComentarios(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, HomeBannersRequest request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("Take={0}&Skip={1}&SearchText={2}&Activo={3}", request.Take, request.Skip, request.SearchText, request.Activo);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<NosotrosComentario>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<NosotrosComentario>>(result);
                return new Response<List<NosotrosComentario>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<NosotrosComentario>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }


        public async Task<Response<List<PrecioHabitacion>>> GetRoomPrice(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {                
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);             

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<PrecioHabitacion>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<PrecioHabitacion>>(result);
                return new Response<List<PrecioHabitacion>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<PrecioHabitacion>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }


        public async Task<Response<List<Contacto>>> GetContact(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<Contacto>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<Contacto>>(result);
                return new Response<List<Contacto>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<Contacto>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<ContactoResponse>> PostContactoMensaje(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, MensajeContacto request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);                                    
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);                
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<ContactoResponse>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<ContactoResponse>(result);
                return new Response<ContactoResponse>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<ContactoResponse>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<List<TabGaleria>>> GetTabsGaleria(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<TabGaleria>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<TabGaleria>>(result);
                return new Response<List<TabGaleria>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<TabGaleria>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }


        public async Task<Response<List<Galeria>>> GetGaleria(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<Galeria>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<Galeria>>(result);
                return new Response<List<Galeria>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<Galeria>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }


        public async Task<Response<Habitacion>> GetHabitacion(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<Habitacion>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var habitacion = JsonConvert.DeserializeObject<Habitacion>(result);
                return new Response<Habitacion>
                {
                    IsSuccess = true,
                    Result = habitacion
                };
            }
            catch (Exception ex)
            {
                return new Response<Habitacion>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }


        public async Task<Response<HabitacionesResponse>> GetRoomsavailable(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, BannersRequest request)
        {
            try
            {
                //var data = new EvaluacionRequest { estatus = 2 };
                //var requestString = JsonConvert.SerializeObject(request);
                //var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                string TotalHabitaciones = "0";
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("CantAdultos={0}&CantNinos={1}&FechaInicio={2}&FechaFinal={3}", request.CantAdultos, request.CantNinos, request.FechaInicio, request.FechaFinal);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<HabitacionesResponse>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }

                try
                {
                    var list = JsonConvert.DeserializeObject<List<Habitacion>>(result);
                    if (response.Headers.Contains("X-Total-Count"))
                    {
                        TotalHabitaciones = response.Headers.GetValues("X-Total-Count").First();
                    }

                    HabitacionesResponse Habitaciones = new HabitacionesResponse()
                    {
                        listaHabitaciones = list,
                        XTotalCount = Int32.Parse(TotalHabitaciones)
                    };

                    return new Response<HabitacionesResponse>
                    {
                        IsSuccess = true,
                        Result = Habitaciones
                    };
                }
                catch
                {
                    return new Response<HabitacionesResponse>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                
               
            }
            catch (Exception ex)
            {
                return new Response<HabitacionesResponse>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<ResponseReservacion>> PostReservacion(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, RequestReservacion request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<ResponseReservacion>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var ResultadoReserva = JsonConvert.DeserializeObject<ResponseReservacion>(result);               
                return new Response<ResponseReservacion>
                {
                    IsSuccess = true,
                    Result = ResultadoReserva
                };  
            }
            catch (Exception ex)
            {
                return new Response<ResponseReservacion>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<DataFullInfo>> GetDatosGeneral(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<DataFullInfo>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<DataFullInfo>(result);
                return new Response<DataFullInfo>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<DataFullInfo>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response<List<ServicioDataPage>>> GetDatosServicios(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<ServicioDataPage>>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<List<ServicioDataPage>>(result);
                return new Response<List<ServicioDataPage>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<List<ServicioDataPage>>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }



        public async Task<Response<List<HotelData>>> GetConexionHotel(string urlBase, string servicePrefix, string controller, string idCodeHotel)
        {
            try
            {
               
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("CodHotel={0}", idCodeHotel);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<HotelData>>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result),
                    };
                }

                var ResultAccess = JsonConvert.DeserializeObject<List<HotelData>>(result);
                return new Response<List<HotelData>>
                {
                    IsSuccess = true,
                    Result = ResultAccess
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<List<HotelData>>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }


        public async Task<Response<ResponsePago>> PostPagarReserva(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, RequestPago request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<ResponsePago>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var ResultadoReserva = JsonConvert.DeserializeObject<ResponsePago>(result);

                if (ResultadoReserva.success == false && !string.IsNullOrEmpty(ResultadoReserva.message))
                {
                    return new Response<ResponsePago>
                    {
                        IsSuccess = true,
                        Result = ResultadoReserva
                    };

                }
                else
                {
                    var reservaTemp = JsonConvert.DeserializeObject<Reserva>(result);
                    return new Response<ResponsePago>
                    {
                        IsSuccess = true,
                        Result = new ResponsePago()
                        {
                            success = true,
                            message = "",
                            reserva = reservaTemp
                        }
                    };
                }


                /*
                return new Response<ResponsePago>
                {
                    IsSuccess = true,
                    Result = ResultadoReserva
                }; */
            }
            catch (Exception ex)
            {
                return new Response<ResponsePago>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }



        public async Task<Response<ResponseConsultaReserva>> GetReservacion(string urlBase, string servicePrefix, string controller, string tokenType, string accessToken, string IdReserva)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlBase}/{servicePrefix}{controller}?id={IdReserva}"; 
                 UriBuilder builder = new UriBuilder(url);
               

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<ResponseConsultaReserva>
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }
                var list = JsonConvert.DeserializeObject<ResponseConsultaReserva>(result);
                return new Response<ResponseConsultaReserva>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response<ResponseConsultaReserva>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }





    }
}
