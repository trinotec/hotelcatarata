﻿using HotelCatarataCore.Web.Models;
using HotelCatarataCore.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Helpers
{
    public sealed class GetAccess
    {
        private  static GetAccess _instance = null;
        private readonly IApiService _apiService;
        private GetAccess(IApiService apiService)
        {
            _apiService = apiService;
        }
        
        public static GetAccess Instance(IApiService apiService)
        {
            if (_instance == null)
            {
                _instance = new GetAccess( apiService);
            }
            return _instance;

        }
              
        public async Task<Access> GetToken()
        {
            Access _dataAccess = new Access();
            var url = Constants.UrlAPI;
            var requestAccess = new AccessRequest()
            {
                Grant_type = "password",
                UserName = "soporte04@sicsoftsa.com",
                Password = "Hola123"
            };

            var responseAccess = await _apiService.GetAccess(url, Constants.UrlAPP, "/Token", requestAccess);
            if (!responseAccess.IsSuccess)
            {
                return null;
            }
            _dataAccess = responseAccess.Result;

            return await Task.FromResult(_dataAccess);
 
        }

    }
}
