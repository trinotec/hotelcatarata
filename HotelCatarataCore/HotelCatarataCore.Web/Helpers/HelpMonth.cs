﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Helpers
{
    static public class HelpMonth
    {
        static public string ObtenerMes(int mes)
        {             
            Dictionary<int, string> Meses = new Dictionary<int, string>();
            Meses.Add(1, "Ene");
            Meses.Add(2, "Feb");
            Meses.Add(3, "Mar");
            Meses.Add(4, "Abr");
            Meses.Add(5, "May");
            Meses.Add(6, "Jun");
            Meses.Add(7, "Jul");
            Meses.Add(8, "Ago");
            Meses.Add(9, "Sep");
            Meses.Add(10, "Oct");
            Meses.Add(11, "Nov");
            Meses.Add(12, "Dic");

            return Meses[mes];
        }
    }
}
