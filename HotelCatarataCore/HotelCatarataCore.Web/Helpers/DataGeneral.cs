﻿using HotelCatarataCore.Web.Models;
using HotelCatarataCore.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelCatarataCore.Web.Helpers
{
    public sealed class DataGeneral
    {

        private static DataGeneral _instance = null;
        private readonly IApiService _apiService;
        private readonly Access _dataAccess;
        
        private DataGeneral(IApiService apiService, Access dataAccess)
        {
            _apiService = apiService;
            _dataAccess = dataAccess;
        }

        public static DataGeneral Instance(IApiService apiService, Access dataAccess)
        {
            if (_instance == null)
            {
                _instance = new DataGeneral(apiService, dataAccess);
            }
            return _instance;

        }

         


    }
}
 