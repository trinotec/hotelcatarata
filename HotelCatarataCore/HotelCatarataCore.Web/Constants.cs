﻿using HotelCatarataCore.Web.Helpers;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace HotelCatarataCore.Web
{
    public class Constants
    {
        private static IConfigurationRoot Configuration { get; } =
         ConfigurationHelper.GetConfiguration(Directory.GetCurrentDirectory());

        public static string UrlAPI { get; } = Configuration.GetSection("ApiSettings")["UrlApi"];
        public static string UrlAPP { get; } = Configuration.GetSection("ApiSettings")["UrlApp"];
        public static string UrlKaptcha { get; } = Configuration.GetSection("ApiSettings")["UrlKaptcha"];

        public static string IdCodeHotel { get; } = Configuration.GetSection("ApiSettings")["idCodeHotel"];

        public static string Grant_type = "password";
        public static string UserName = "soporte04@sicsoftsa.com";
        public static string Password = "Hola123";
    }
}
