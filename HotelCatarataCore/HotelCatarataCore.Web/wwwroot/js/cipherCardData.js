// Inicia la librería RSA
const rsa_ = new JSEncrypt()

/**
 * Carga la llave publica enviada por GreenPay en el archivo de credenciales
 * Se deben elimnar los saltos de lines y los dashes -----BEGIN PUBLIC KEY----- y -----END PUBLIC KEY-----
 * TODO : reemplazar valores
 */
rsa_.setPublicKey(
    //"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCZDfMe1oZkYod5nrhADJcxfqsKwtyKETN8B4Lj6wfYN1nhXSv14g1HMSr3kFE/ofx0jo+hX7S4R5HFWGZLzxFskrK/7UhOo0zUBqWxz6q0ozAeYsQYiT7XsiWXv4DoJA80t8p0RPaZh6+JmtzmFN0RWt8l7cfTtRnTnT1LfFD8PwIDAQAB"
    "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCSep/jgS7RKW12nNkuMLVoJt45ZlGXckQc0nep4XdTpl+7ccQ88ixKIyIhjcY+TFayhDdXXbl/l5nMOKeCGYuz26Uc3ASV/yMgkMXmBUX6apRrr0B58+4v4c4213N07dShd+U5W7FB6LKdgkaSmSE9ijJAqyBGxyJlc5u1sItqrwIDAQAB"
);

//Genera el par de llaves AES utilizados para cifrar el JSON con los datos de la tarjeta
function generateAESPairs() {
    var key = [];
    var iv;
    for (let k = 0; k < 16; k++) {
        key.push(Math.floor(Math.random() * 255));
    }
    for (let k = 0; k < 16; k++) {
        iv = Math.floor(Math.random() * 255);
    }

    return {
        k: key,
        s: iv
    };
}

//Creates an JSON object with the card data and AES key Pair encrypted
function pack(obj, session, pair_) {
    var pair = (pair_ !== undefined) ? pair_ : generateAESPairs();

    var textBytes = aesjs.utils.utf8.toBytes(JSON.stringify(obj));
    var aesCtr = new aesjs.ModeOfOperation.ctr(pair.k, new aesjs.Counter(pair.s));
    var encryptedBytes = aesCtr.encrypt(textBytes);
    var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);

    var returnObj = {
        session: session,
        ld: encryptedHex,
        lk: rsa_.encrypt(JSON.stringify(pair))
    };

    return returnObj;
}

/**
 * JSON recibido al crear una orden de checkout en https://sandbox-merchant.greenpay.me
 * ver el árticulo https://support.greenpay.me/portal/kb/articles/crear-una-orden-de-pago-8-10-2019
 * para crear una orden de checkout
 * TODO : reemplazar valores
 */
//const greenpayOrderCreated = {
//    "session": "bc4b2517-a2d9-4149-a379-ab05fa393039",
//    "token": "4adbd38f-45bf-411e-9551-d75f9610f687"
//}

///**
// * JSON con los datos de las tarjeta a cifrar
// * TODO : reemplazar valores
//// */
//const cardData = {
//    "card": {
//        "cardHolder": "Test",
//        "expirationDate": {
//            "month": 9,
//            "year": 22
//        },
//        "cardNumber": "5474560500238182",
//        "cvc": "162"
//    },
//    "kountSession": "L3ON4RD0arc3M3j14p09iokl3E4R5T64"
//};

//// Objeto a enviar al API de checkout
//var result = pack(cardData, greenpayOrderCreated.session);

//console.log(result);
//alert(JSON.stringify(result, null, 2));
