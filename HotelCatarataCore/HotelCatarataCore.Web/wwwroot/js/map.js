function initMap() {
    var latlng = new google.maps.LatLng($('#LAT').val(), $('#LON').val());
    var latlng2 = new google.maps.LatLng($('#LAT').val(), $('#LON').val());
    var imglogo = $("#urlLogoHddn").val();
 

    var myOptions = {
        zoom: 10,
        center: latlng,
        styles: [
    {
        "featureType": "administrative",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit.station.rail",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "saturation": "-100"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    }
]

    };

    var map = new google.maps.Map(document.getElementById("map"), myOptions);

    var myMarker = new google.maps.Marker(
        {            
            position: latlng,
            map: map,            
            title: $('#NAME').val()
        });




}