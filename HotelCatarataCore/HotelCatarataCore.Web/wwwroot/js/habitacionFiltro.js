﻿var textoFiltro = "Control";
var NumHabPag = 6;
$(document).ready(function () {
	ConsultaFiltro(1)
});

function ConsultaFiltro(NumPagina) {

	var PrecioFiltro = $(".select-selected").html();
	if (textoFiltro == "") {
		return;
	}

	$.ajax({
		//url: "/Habitaciones/BusquedaHabitaciones?NumPagina=" + NumPagina + "&PrecioFiltro=" + PrecioFiltro + "&NumHabPag=" + NumHabPag,
		url: $('#URL_BASE').val() + "Habitaciones/BusquedaHabitaciones?NumPagina=" + NumPagina + "&PrecioFiltro=" + PrecioFiltro + "&NumHabPag=" + NumHabPag,
		success: function (data) {			
			var html = "";
			console.log(data);
			if (data.IsSuccess == true) {
				GenerarHtmlHabitaciones(data.Result, NumPagina)
				Swal.close();
			}
			else {
				Swal.close();
			}

		},
		beforeSend: function () {
			Swal.fire({
				allowOutsideClick: false,
				showConfirmButton: false,
				showClass: {
					popup: 'animated fadeIn faster'
				},
				customClass: {
					container: 'sweetLoadingcontent'
				},
				html: "<div class='animacionJson'><lottie-player src='" + $('#URL_BASE').val() +"js/LoadingcataratasGris.json' background='transparent' speed='1' style='width: 150px; height: 150px;' loop  autoplay>" +
					"</lottie-player></div>"
			});
		}
	}).done(function () {
		$.holdReady(true);
		$.getScript($('#URL_BASE').val() + "js/lib/slick/slick.js", function () {
			$.holdReady(false);
		});

	});
}

function GenerarHtmlHabitaciones(resultado, NumPagina) {

	//---------------------------------------------------
	// Para la primera vista del habitaciones
	var htmlHabitaciones1 = "";
	for (var i = 0; i < resultado.listaHabitaciones.length; i++) {
		htmlHabitaciones1 = htmlHabitaciones1 +
			"<div class='col-lg-4 col-md-6'>" +
			"	<div class='room_details CardVertical'>" +
			"		<div class='room-caroz'>";

		for (var j = 0; j < resultado.listaHabitaciones[i].Fotos.length; j++) {
			htmlHabitaciones1 = htmlHabitaciones1 +
				"			<div class='room_img'>" +
				"				<img src='" + resultado.listaHabitaciones[i].Fotos[j].Imagen + "' alt=''>" +
				"				<span class='price-tag'> $" + (resultado.listaHabitaciones[i].Precio).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString() + "<b>/Noche</b></span>" +
				"			</div><!--room_img end-->";
		}

		htmlHabitaciones1 = htmlHabitaciones1 +
			"		</div><!--room-caroz end-->" +
			"		<div class='room_info'>" +
			"			<h3 class='one-line-truncate'><a href='javascript:;'  onclick='reservarHabitacion(" + resultado.listaHabitaciones[i].CodTipoHab +")' title=''>" + resultado.listaHabitaciones[i].NombreTipoHab + "</a></h3>" +
			"			<p class='multi-line-truncate'>" + resultado.listaHabitaciones[i].Descripcion + "</p>" +
			"			<ul class='fct-list h-70'>";

		for (var k = 0; k < resultado.listaHabitaciones[i].Servicios.length; k++) {
			htmlHabitaciones1 = htmlHabitaciones1 +
				"				<li>" +
				"					<span data-toggle='tooltip' data-placement='top' title='" + resultado.listaHabitaciones[i].Servicios[k].NomServicio + "'><img style='width: 100%;' src='" + resultado.listaHabitaciones[i].Servicios[k].IconoFullPath + "' alt=''></span>" +
				"				</li>";
		}

		htmlHabitaciones1 = htmlHabitaciones1 +
			"			</ul><!--fct-list end-->" +
			"			<a href='javascript:;' title='' class='lnk-default' onclick='reservarHabitacion(" + resultado.listaHabitaciones[i].CodTipoHab +")'>VER HABITACIÓN<i class='la la-arrow-right'></i></a>" +
			"		</div><!--room_info end-->" +
			"	</div><!--room-details end-->" +
			"</div>";
	}
	$("#rowHab1").html(htmlHabitaciones1);

	//---------------------------------------------------
	// Para la segunda vista del habitaciones
	var htmlHabitaciones2 = "";
	for (var i = 0; i < resultado.listaHabitaciones.length; i++) {
		htmlHabitaciones2 = htmlHabitaciones2 +
			"<div class='tb-row ' >" +
			"	<div class='room_details'>" +
			"		<div class='room-caroz'>";

		for (var j = 0; j < resultado.listaHabitaciones[i].Fotos.length; j++) {
			htmlHabitaciones2 = htmlHabitaciones2 +
				"			<div class='room_img'>" +
				"				<img src='" + resultado.listaHabitaciones[i].Fotos[j].Imagen + "' alt=''>" +
				"				<span class='price-tag'>$" + (resultado.listaHabitaciones[i].Precio).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString() + "<b>/Noche</b></span>" +
				"			</div><!--room_img end-->";
		}

		htmlHabitaciones2 = htmlHabitaciones2 +
			"		</div><!--room-caroz end-->" +
			"		<div class='room_info'>" +
			"			<h3 class='one-line-truncate'><a  href='javascript:;'  onclick='reservarHabitacion(" + resultado.listaHabitaciones[i].CodTipoHab +")'>" + resultado.listaHabitaciones[i].NombreTipoHab + "</a></h3>" +
			"			<p class='multi-line-truncate list'>" + resultado.listaHabitaciones[i].Descripcion + " </p>" +
			"			<div class='fct-dv'>" +
			"				<ul class='fct-list'>";

		for (var k = 0; k < resultado.listaHabitaciones[i].Servicios.length; k++) {
			htmlHabitaciones2 = htmlHabitaciones2 +
				"					<li>" +
				"						<span data-toggle='tooltip' data-placement='top' title='" + resultado.listaHabitaciones[i].Servicios[k].NomServicio + "'><img style='width: 100%;' src='" + resultado.listaHabitaciones[i].Servicios[k].IconoFullPath + "' alt=''></span>" +
				"					</li>";
		}

		htmlHabitaciones2 = htmlHabitaciones2 +
			"				</ul><!--fct-list end-->" +
			"				<a href='javascript:;' title='' class='lnk-default' onclick='reservarHabitacion(" + resultado.listaHabitaciones[i].CodTipoHab +")' >VER HABITACIÓN<i class='la la-arrow-right'></i></a>" +
			"				<div class='clearfix'></div>" +
			"			</div>" +
			"		</div><!--room_info end-->" +
			"		<div class='clearfix'></div>" +
			"	</div><!--room-details end-->" +
			"</div><!--tb-row end-->";
	}

	$("#rowHab2").html(htmlHabitaciones2);

	//---------------------------------------------------
	// Para la paginacion

	if (resultado.XTotalCount > NumHabPag) {

		console.log(resultado.XTotalCount);
		var htmlPaginacion = "";
		for (var i = 1; i <= Math.ceil(resultado.XTotalCount / NumHabPag); i++) {
			var active = (i == NumPagina) ? "active" : "";
			htmlPaginacion = htmlPaginacion + "<li class='page-item " + active + "'><a class='page-link' onClick='ConsultaFiltro(" + i + ")')' style='cursor: pointer;' >" + i + "</a></li>";
		}
		$("#HabPaginacion").html(htmlPaginacion);

	}
	else {
		$("#HabPaginacion").html("");
	}
}

$("body").on('DOMSubtreeModified', ".select-selected", function () {
	textoFiltro = $(".select-selected").html();
	ConsultaFiltro(1);
});